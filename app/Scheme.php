<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scheme extends Model
{
    public function units()
    {
        return $this->hasMany(Unit::class);
    }
}
