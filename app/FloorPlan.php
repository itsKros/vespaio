<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FloorPlan extends Model
{
    protected $fillable = ['name', 'type', 'alias', 'bdbth', 'rent_range_min', 'rent_range_max', 'deposit', 'term_min', 'term_max', 'app_fee',
        'dog_deposit', 'cat_deposit', 'pet_rent', 'plan_intro', 'feature'];

    public function units()
    {
        return $this->hasMany(Unit::class);
    }
}
