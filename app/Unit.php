<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
<<<<<<< HEAD
    protected $fillable = ['name', 'show', 'list_1st', 'date_available', 'rent', 'floor_plan_id', 'scheme_id',
        'view_id', 'floor_no', 'feature_1', 'feature_2', 'feature_3', 'sqft'];
=======
    protected $fillable = ['name', 'show', 'list_1st', 'date_available', 'rent', 'sqft', 'floor_plan_id', 'scheme_id',
        'view_id', 'floor_no', 'feature_1', 'feature_2', 'feature_3'];
>>>>>>> a3e898390069121c6ee85957372f45375043fd5a

    public function floor_plan()
    {
        return $this->belongsTo(FloorPlan::class);
    }

    public function scheme()
    {
        return $this->belongsTo(Scheme::class);
    }

    public function view()
    {
        return $this->belongsTo(View::class);
    }

    public function getDateAvailableAttribute($date)
    {
        if(!empty($date))
            return Carbon::parse($date)->format('m/d/Y');
        return null;
    }
}
