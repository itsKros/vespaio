<?php

namespace App\Http\Controllers\Backend;
use App\FloorPlan;
use App\Scheme;
use App\Unit;
use App\User;
use App\Http\Controllers\Controller;
use App\View;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $units_by_plan_type = $this->get_units_by_plan_type();

        $units_by_plan_name = $this->get_units_by_plan_name();

        $units_by_rent_range = $this->get_units_by_rent_range();

        $units_by_floor_no = $this->get_units_by_floor_no();

        $units_by_view = $this->get_units_by_view();

        $units_by_scheme = $this->get_units_by_scheme();

        return view('backend.home', compact('units_by_plan_type', 'units_by_rent_range',
            'units_by_plan_name', 'units_by_floor_no', 'units_by_view', 'units_by_scheme'));
    }

    public function get_units_by_plan_type():array
    {
        $units_by_plan_type_all = array();
        $units_by_plan_type = FloorPlan::withCount('units')->groupBy('type')->get()->pluck('units_count', 'type');

        $all = FloorPlan::withCount('units')->get()->sum('units_count');

        $units_by_plan_type = $units_by_plan_type->toArray();

        $arr = [];
        foreach ($units_by_plan_type as $key=>$type){
            $arr[] = array($key, $type);
        }

        $units_by_plan_type = $arr;

        $units_by_plan_type_all[] = array('Plan Type', 'All Plan Type');
        $units_by_plan_type_all[] = array('All', $all);

        $units_by_plan_type = array_merge($units_by_plan_type_all, $units_by_plan_type);

         return $units_by_plan_type;
    }

    public function get_units_by_rent_range():array
    {
        $rent_min = round(FloorPlan::min('rent_range_min'), -3);
        $rent_max = round(FloorPlan::max('rent_range_min'), -3);
        $units_by_rent_range = array();
        $units_by_rent_range[] = array('Rent Range', 'All Rent Range');
        $units_by_rent_range[] = array('All', Unit::where('rent', '>', 0)->count());
        for($i=$rent_min; $i<=$rent_max; $i+=500){
            $units_count = FloorPlan::whereHas('units', function($query) use($i){
                $query->where([['rent', '>=', $i], ['rent', '<', $i+500]]);
            })->count();
            $units_by_rent_range[] = array('<'.($i+500), $units_count);
        }
        return $units_by_rent_range;
    }

    public function get_units_by_plan_name():array
    {
        $units_by_plan_name = FloorPlan::withCount('units')->get()->pluck('units_count', 'name');

        $units_by_plan_name = $units_by_plan_name->toArray();

        $arr = [];
        foreach ($units_by_plan_name as $key=>$name){
            $arr[] = array($key, $name);
        }

        $units_by_plan_name = $arr;

        $units_by_plan_name_all[] = array('Plan Name', 'All Plan Name');

        $units_by_plan_name= array_merge($units_by_plan_name_all, $units_by_plan_name);

        return $units_by_plan_name;
    }

    public function get_units_by_floor_no():array
    {
        $units_by_floor_no = array();
        $units_by_floor_no[] = array('Floor No', 'All Floor No');
        for($i=3; $i<=7; $i++){
            $units_count = Unit::where('floor_no', $i)->count();
            $units_by_floor_no[] = array($i.'', $units_count);
        }
        return $units_by_floor_no;
    }

    public function get_units_by_view()
    {
        $units_by_view = View::withCount('units')->get()->pluck('units_count', 'name')->toArray();

        $arr = array();
        $arr[] = array('View', 'All View');
        foreach ($units_by_view as $key=>$count){
            $arr[] = array($key, $count);
        }
        $units_by_view = $arr;

        return $units_by_view;
    }

    public function get_units_by_scheme()
    {
        $units_by_scheme = Scheme::withCount('units')->get()->pluck('units_count', 'name')->toArray();

        $arr = array();
        $arr[] = array('Scheme', 'All Scheme');
        foreach ($units_by_scheme as $key=>$count){
            $arr[] = array($key, $count);
        }
        $units_by_scheme = $arr;

        return $units_by_scheme;
    }
}
