<?php

namespace App\Http\Controllers\Backend;

use App\FloorPlan;
use App\Notifications\UserRegistered;
use App\Scheme;
use App\Unit;
use App\View;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

class UnitsController extends Controller
{
    private $screen_height = 0;
    private $num_rows = 5;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->screen_height = (int) Cookie::get('screenHeight');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        if(!empty($this->screen_height)){
            $this->screen_height = $this->screen_height - 240;
            $this->num_rows = floor($this->screen_height/37) - 1;
        }

        $query = Unit::select('*');

        if(!empty($request)){
            if($request->unit_check==1){
                $query = $query->where('date_available', '!=', null);
            }

            if(!empty($request->name)){
                $query = $query->where('name', 'like', '%'.$request->name.'%');
            }

            if(!empty($request->floor_no)){
                $query = $query->where('floor_no', $request->floor_no);
            }

            if(!empty($request->view_id)){
                $query = $query->where('view_id', $request->view_id);
            }

            if(!empty($request->scheme_id)){
                $query = $query->where('scheme_id', $request->scheme_id);
            }
        }

        $units = $query->paginate($this->num_rows)->appends($request->all());

        $schemes = Scheme::all();

        $floor_plans = FloorPlan::all();

        $views = View::all();

        return view('backend.units.index', compact('units', 'schemes', 'floor_plans', 'views'));
    }

   /* public function filter(Request $request)
    {
        $query = Unit::select('*');

        if($request->unit_check==1){
            $query = $query->where('date_available', '!=', null);
        }

        if(!empty($request->name)){
            $query = $query->where('name', 'like', '%'.$request->name.'%');
        }

        if(!empty($request->floor_no)){
            $query = $query->where('floor_no', $request->floor_no);
        }

        if(!empty($request->view_id)){
            $query = $query->where('view_id', $request->view_id);
        }

        if(!empty($request->scheme_id)){
            $query = $query->where('scheme_id', $request->scheme_id);
        }

        $units = $query->paginate($this->num_rows);

        return redirect()->route('backend.units.index')->with('units', $units)->withInput();
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules =  [
            'name.*'            => 'required|nullable|string|min:3',
            'rent.*'            => 'present|nullable|integer|min:1',
            'floor_no.*'        => 'present|nullable|integer|min:3|max:7',
            'feature_1.*'       => 'present|nullable|string',
            'feature_2.*'       => 'present|nullable|string',
            'feature_3.*'       => 'present|nullable|string'
        ];

        $this->validate($request, $rules);

        $ids = $request->id;

        $units = [];
        foreach($ids as $index=>$id){
            $unit = Unit::find($id);

            $date_available = $request->date_available[$index];
            if($request->date_available[$index] === 'Now' || $request->date_available[$index] === 'Leased'){
                $date_available =  $unit->date_available;
            }

            $units[] = $unit->update([
                'name'              => $request->name[$index],
                'show'              => in_array($id, $request->show, false)? 1 : 0,
                'list_1st'          => in_array($id, $request->list_1st, false)? 1 : 0,
                'date_available'    => empty($date_available)? null : date('Y-m-d', strtotime($date_available)),
                'rent'              => $request->rent[$index],
                'floor_plan_id'     => $request->floor_plan_id[$index],
                'scheme_id'         => $request->scheme_id[$index],
                'view_id'           => $request->view_id[$index],
                'floor_no'          => $request->floor_no[$index],
                'feature_1'         => $request->feature_1[$index],
                'feature_2'         => $request->feature_2[$index],
                'feature_3'         => $request->feature_3[$index],
            ]);
        }

        if(count($units) === count($ids)){
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Units was successfully saved!');
        }else{
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'There was an error saving units!');
        }

        return redirect()->route('backend.units.filter');
    }
}
