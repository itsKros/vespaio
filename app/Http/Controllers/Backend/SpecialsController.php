<?php

namespace App\Http\Controllers\Backend;

use App\Special;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SpecialsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specials = Special::find(1);

        return view('backend.specials.index', compact('specials'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Special $special)
    {
        $special->specials = $request->specials;
        $special->leasing_terms = $request->leasing_terms;
        $special->pet_policy = $request->pet_policy;
        $special->parking_policy = $request->parking_policy;
        $special->show_specials = isset($request->show_specials)? 1 : 0;

        if($special->save()){
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Specials was successfully updated!');
        }else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'There was an error updating specials!');
        }

        return redirect()->route('backend.specials.index');
    }
}
