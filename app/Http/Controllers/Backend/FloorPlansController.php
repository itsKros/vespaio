<?php

namespace App\Http\Controllers\Backend;

use App\Feature;
use App\FloorPlan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use function PHPSTORM_META\map;

class FloorPlansController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $floor_plans = FloorPlan::all();

        $rent_ranges = FloorPlan::select(DB::raw("CONCAT(rent_range_min,' - ',rent_range_max) AS rent_range"), 'id')->pluck('rent_range', 'id');
        $terms = FloorPlan::select(DB::raw("CONCAT(term_min,' - ',term_max) AS term"), 'id')->pluck('term', 'id');
        $names = $floor_plans->pluck('name', 'id');
        $deposits = $floor_plans->pluck('deposit', 'id');
        $app_fees = $floor_plans->pluck('app_fee', 'id');
        $dog_deposits = $floor_plans->pluck('dog_deposit', 'id');
        $cat_deposits = $floor_plans->pluck('cat_deposit', 'id');
        $pet_rents = $floor_plans->pluck('pet_rent', 'id');
        $plan_intros = $floor_plans->pluck('plan_intro', 'id');
        $features = $floor_plans->pluck('feature', 'id');

        return view('backend.floor_plans.index', compact('floor_plans', 'names', 'rent_ranges',
            'deposits', 'terms', 'app_fees', 'dog_deposits', 'cat_deposits', 'pet_rents', 'plan_intros', 'features'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules =  [
            'rent_range.*'          => 'required|string|min:3',
            'deposit.*'             => 'required|integer',
            'term.*'                => 'required|string|min:3',
            'app_fee.*'             =>'required|integer',
            'dog_deposit.*'         =>'required|integer',
            'cat_deposit.*'         =>'required|integer',
            'pet_rent.*'            =>'required|integer',
            'plan_intro.*'          =>'required|string'
        ];

        $this->validate($request, $rules);

        /*foreach($request->rent_range as $range){
            $ranges = explode('-', $range);
            if(count($ranges)!==2 || !ctype_digit(trim($ranges[0])) || !ctype_digit(trim($ranges[1]))){
                $request->session()->flash('message.level', 'danger');
                $request->session()->flash('message.content', 'The rent range must be integers separated by a hyphen (e.g 1000 - 2000).');
                return redirect()->route('backend.floor-plans.index');
            }
        }*/

        foreach($request->term as $term){
            $terms = explode('-', $term);
            if(count($terms)!==2 || !ctype_digit(trim($terms[0])) || !ctype_digit(trim($terms[1]))){
                $request->session()->flash('message.level', 'danger');
                $request->session()->flash('message.content', 'The term must be integers separated by a hyphen (e.g 6 - 12).');
                return redirect()->route('backend.floor-plans.index');
            }
        }

        $ids = $request->id;
       // $rent_ranges = $request->rent_range;
        $deposits = $request->deposit;
        $terms = $request->term;
        $app_fees = $request->app_fee;
        $dog_deposits = $request->dog_deposit;
        $cat_deposits = $request->cat_deposit;
        $pet_rents = $request->pet_rent;
        $plan_intros = $request->plan_intro;
        $features = $request->feature;

        foreach ($ids as $index=>$id){
            $floor_plan = FloorPlan::find($id);

            $floor_plan->update([
               /* 'rent_range_min' => trim(explode('-', $rent_ranges[$index])[0]),
                'rent_range_max' => trim(explode('-', $rent_ranges[$index])[1]),*/
                'deposit' => $deposits[$index],
                'term_min' => trim(explode('-', $terms[$index])[0]),
                'term_max' => trim(explode('-', $terms[$index])[1]),
                'app_fee'  =>  $app_fees[$index],
                'dog_deposit' => $dog_deposits[$index],
                'cat_deposit' => $cat_deposits[$index],
                'pet_rent' => $pet_rents[$index],
                'plan_intro' => $plan_intros[$index],
                'feature' => $features[$index]
            ]);
        }

        if($floor_plan){
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Floor Plans successfully updated!');
        }else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'There was an error updating floor plans!');
        }

        return redirect()->route('backend.floor-plans.index');
    }
}
