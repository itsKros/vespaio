<?php

namespace App\Http\Controllers\Backend;

use App\Notifications\UserRegistered;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('new_password', 'new_password_save');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();

        $users = User::all();
        
        return view('backend.users.index', compact('users', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required|string|min:3',
            'email'         => 'required|email|confirmed|unique:users,email',
            'role_id'       => 'required|integer',
            'receive_email' =>'required|integer|digits_between:0,1'
        ]);

        $user = User::create([
            'name'          => $request->name,
            'email'         => $request->email,
            'role_id'       => $request->role_id,
            'receive_email' => $request->receive_email,
            'password'      => null,
            'new_password_token' => sha1(time())
        ]);

        if($user){
            $user->notify(new UserRegistered($user));
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'User was successfully created!');
        }else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'There was an error creating user!');
        }

        return redirect()->route('backend.users.index');
    }

    public function new_password(String $token)
    {
        if(!empty($token) ){
            $user = User::where('new_password_token', $token)->first();
            if(empty($user)) abort(404);
            return view('auth.passwords.new', compact('token'));
        }else{
            abort(404);
        }
    }

    public function new_password_save()
    {
        $this->validate(request(), [
            'email'         => 'required|email',
            'password'      => 'required|confirmed',
        ]);

        $user = User::where(['email'=>request()->email, 'new_password_token'=>request()->token])->first();

        if(empty($user)){
            request()->session()->flash('message.level', 'danger');
            request()->session()->flash('message.content', 'Please check information you entered!');
            return redirect()->route('backend.users.new_password', ['token' => request()->token]);
        }
        $user->password = bcrypt(request()->password);
        $user->new_password_token = null;

        if($user->save()){
            request()->session()->flash('message.level', 'success');
            request()->session()->flash('message.content', 'New Password was successfully saved!');
        }else{
            request()->session()->flash('message.level', 'danger');
            request()->session()->flash('message.content', 'There was an error saving password, please try again!');
        }

        return redirect()->route('login');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules =  [
            'name.*'          => 'required|string|min:3',
            'email.*'         => 'required|email',
            'role_id.*'       => 'required|integer',
            'receive_email.*' =>'required|integer|digits_between:0,1'
        ];

        $this->validate($request, $rules);

        $ids = $request->id;

        $users = $request->all();

        foreach ($ids as $index=>$id){
            $user = User::find($id);
            $user->update([
                'name' => $users['name'][$index],
                'email' => $users['email'][$index],
                'role_id' => $users['role_id'][$index],
                'receive_emails' => $users['receive_email'][$index]
            ]);
        }

        if($users){
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Users successfully updated!');
        }else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'There was an error updating users!');
        }

        return redirect()->route('backend.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if($user->delete()){
            request()->session()->flash('message.level', 'success');
            request()->session()->flash('message.content', 'User was successfully deleted!');
        } else {
            request()->session()->flash('message.level', 'danger');
            request()->session()->flash('message.content', 'There was an error deleting user!');
        }

        return redirect()->route('backend.users.index');
    }
}
