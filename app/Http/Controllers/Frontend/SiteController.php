<?php

namespace App\Http\Controllers\Frontend;
use Jenssegers\Agent\Agent;
use App\FloorPlan;
use App\Http\Controllers\Controller;
use App\Scheme;
use App\Special;
use App\Unit;
use App\View;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;


class SiteController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
    **/
    public function index()
    {
        $specials = Special::find(1);

        $units = Unit::with('floor_plan', 'scheme', 'view')->get();

        $floor_plans = FloorPlan::all()->toArray();
        $floor_plans = array_map(function($plan){
            $plan['checked'] = false;
            return $plan;
        }, $floor_plans);
        $floor_plans = Arr::prepend($floor_plans, [
            'id'=>0,
            'name'=>'All',
            'alias'=> 'Plans',
            'bdbth'=> 'Plans',
            'checked' => true
        ]);

        $views = View::all()->toArray();
        $views = array_map(function($view){
            $view['checked'] = false;
            return $view;
        }, $views);
        $views = Arr::prepend($views, [
            'id' => 0,
            'name'=>'All Views',
            'checked' => true
        ]);

        $schemes = Scheme::all()->toArray();
        $schemes = array_map(function($scheme){
            $scheme['checked'] = false;
            return $scheme;
        }, $schemes);
        $schemes = Arr::prepend($schemes, [
            'id' => 0,
            'name'=>'All Schemes',
            'checked' => true
        ]);

        $floors = [['id' => 0, 'name' => 'All Floors', 'checked'=> true],['id' => 3, 'name' => 'Third', 'checked'=> false], ['id' => 4, 'name' => 'Fourth', 'checked'=> false],['id' => 5, 'name' => 'Fifth', 'checked'=> false],['id' => 6, 'name' => 'Sixth', 'checked'=> false],['id' => 7, 'name' => 'Seventh', 'checked'=> false]];

        $periods = [['id'=>'0', 'name'=>'All', 'checked'=> true], ['id'=>'1', 'name'=>'Now', 'checked'=> false], ['id'=>'1-30', 'name'=>'1-30 Days', 'checked'=> false], ['id'=>'31-60', 'name'=>'30-60 Days', 'checked'=> false], ['id'=>'61-90', 'name'=>'61-90 Days', 'checked'=> false], ['id'=>'91-120', 'name'=>'91-120 Days', 'checked'=> false], ['id'=>'120-0', 'name'=>'120+ Days', 'checked'=> false]];

        $rent_ranges = [['id'=>'0', 'name'=>'All', 'checked'=> true], ['id'=>'0-2500', 'name'=>'<2500', 'checked'=> false], ['id'=>'2500-3000', 'name'=>'<3000', 'checked'=> false], ['id'=>'3000-3500', 'name'=>'<3500', 'checked'=> false], ['id'=>'3500-4000', 'name'=>'<4000', 'checked'=> false], ['id'=>'4000-4500', 'name'=>'<4500', 'checked'=> false], ['id'=>'4500-0', 'name'=>'4500+', 'checked'=> false]];

        $agent = new Agent();
        return view('frontend.home', compact('agent', 'specials', 'units', 'floor_plans', 'views', 'schemes', 'floors', 'periods', 'rent_ranges','specials'));
    }

}
