<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    public function units()
    {
        return $this->hasMany(Unit::class);
    }
}
