<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Special extends Model
{
    protected $fillable = ['specials', 'leasing_terms', 'pet_policy', 'parking_policy', 'show_special'];
}
