-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 01, 2019 at 05:25 AM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vespaio`
--

-- --------------------------------------------------------

--
-- Table structure for table `floor_plans`
--

DROP TABLE IF EXISTS `floor_plans`;
CREATE TABLE IF NOT EXISTS `floor_plans` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(3) NOT NULL,
  `type` varchar(10) NOT NULL,
  `alias` varchar(10) NOT NULL,
  `bdbth` varchar(10) NOT NULL,
  `rent_range_min` int(10) UNSIGNED NOT NULL,
  `rent_range_max` int(10) UNSIGNED NOT NULL,
  `deposit` int(10) UNSIGNED NOT NULL,
  `term_min` int(10) UNSIGNED NOT NULL,
  `term_max` int(10) UNSIGNED NOT NULL,
  `app_fee` int(10) UNSIGNED NOT NULL,
  `dog_deposit` int(10) UNSIGNED NOT NULL,
  `cat_deposit` int(10) UNSIGNED NOT NULL,
  `pet_rent` int(10) UNSIGNED NOT NULL,
  `plan_intro` text NOT NULL,
  `feature` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `floor_plans`
--

INSERT INTO `floor_plans` (`id`, `name`, `type`, `alias`, `bdbth`, `rent_range_min`, `rent_range_max`, `deposit`, `term_min`, `term_max`, `app_fee`, `dog_deposit`, `cat_deposit`, `pet_rent`, `plan_intro`, `feature`, `created_at`, `updated_at`) VALUES
(1, 'S1', 'Studio', 'Studio', 'Studio', 2000, 2500, 60, 6, 12, 60, 500, 300, 40, 'Spacious floor plan with lots of natural light and high ceiiling. This is the intro paragraph and it has different line spacing than the bulleted items.', 'All major kitchen appliances \r\nincluded.\r\n\r\nGranite counter tops.\r\n\r\nPlank flooring.\r\n\r\nAir Conditioning.\r\n\r\nGated Parking.', '2019-05-18 05:34:39', '2019-06-01 11:59:46'),
(2, 'S2', 'Studio', 'Studio', 'Studio', 2100, 2500, 70, 6, 14, 70, 600, 400, 40, 'Spacious floor plan with lots of natural light and high ceiiling. This is the intro paragraph and it has different line spacing than the bulleted items.', 'All major kitchen appliances included.\r\nGranite counter tops.', '2019-05-18 05:34:39', '2019-06-01 11:59:49'),
(3, 'A1', '1Bd', '1Bd|1Bth', '1X1', 3000, 3500, 80, 6, 16, 80, 700, 500, 40, 'Spacious floor plan with lots of natural light and high ceiiling. This is the intro paragraph and it has different line spacing than the bulleted items.', '', '2019-05-18 05:34:39', '2019-06-01 12:00:05'),
(4, 'A2', '1Bd', '1Bd|1Bth', '1X1', 3000, 3500, 90, 6, 18, 90, 800, 600, 40, 'Spacious floor plan with lots of natural light and high ceiiling. This is the intro paragraph and it has different line spacing than the bulleted items.', '', '2019-05-18 05:34:39', '2019-06-01 12:00:12'),
(5, 'B1', '2Bd', '2Bd|2Bth', '2X2', 4000, 4500, 100, 6, 20, 100, 900, 700, 40, 'Spacious floor plan with lots of natural light and high ceiiling. This is the intro paragraph and it has different line spacing than the bulleted items.', '', '2019-05-18 05:34:39', '2019-06-01 12:00:19'),
(6, 'B2', '2Bd', '2Bd|2Bth', '2X2', 4000, 4500, 60, 6, 12, 60, 1000, 800, 40, 'Spacious floor plan with lots of natural light and high ceiiling. This is the intro paragraph and it has different line spacing than the bulleted items.', '', '2019-05-18 05:34:39', '2019-06-01 12:00:22'),
(7, 'B3', '2Bd', '2Bd|2Bth', '2X2', 5000, 5500, 60, 6, 12, 60, 500, 300, 40, 'Spacious floor plan with lots of natural light and high ceiiling. This is the intro paragraph and it has different line spacing than the bulleted items.', '', '2019-05-18 05:34:39', '2019-06-01 12:01:18'),
(8, 'B4', '2Bd', '2Bd|2Bth', '2X2', 6000, 6500, 60, 6, 12, 60, 500, 300, 40, 'Spacious floor plan with lots of natural light and high ceiiling. This is the intro paragraph and it has different line spacing than the bulleted items.', '', '2019-05-18 05:34:39', '2019-06-01 12:01:19'),
(9, 'B5', '2Bd', '2Bd|2Bth', '2X2', 6000, 6500, 60, 6, 12, 60, 500, 300, 40, 'Spacious floor plan with lots of natural light and high ceiiling. This is the intro paragraph and it has different line spacing than the bulleted items.', '', '2019-05-18 05:34:39', '2019-06-01 12:01:19'),
(10, 'B6', '2Bd', '2Bd|2Bth', '2X2', 7000, 7500, 60, 6, 12, 60, 500, 300, 40, 'Spacious floor plan with lots of natural light and high ceiiling. This is the intro paragraph and it has different line spacing than the bulleted items.', '', '2019-05-18 05:34:39', '2019-06-01 12:01:21'),
(11, 'C1', '3Bd', '3Bd|2Bth', '3X2', 8000, 8500, 60, 6, 12, 60, 500, 300, 40, 'Spacious floor plan with lots of natural light and high ceiiling. This is the intro paragraph and it has different line spacing than the bulleted items.', '', '2019-05-18 05:34:39', '2019-06-01 12:01:25');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(10, '2014_10_12_000000_create_users_table', 1),
(11, '2014_10_12_100000_create_password_resets_table', 1),
(12, '2019_05_10_131417_create_roles_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', NULL, NULL),
(2, 'Leasing Agent', NULL, NULL),
(3, 'Property Manager', NULL, NULL),
(4, 'Assitant Manager', NULL, NULL),
(5, 'Executive', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `schemes`
--

DROP TABLE IF EXISTS `schemes`;
CREATE TABLE IF NOT EXISTS `schemes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schemes`
--

INSERT INTO `schemes` (`id`, `name`) VALUES
(1, 'Light'),
(2, 'Medium'),
(3, 'Dark');

-- --------------------------------------------------------

--
-- Table structure for table `specials`
--

DROP TABLE IF EXISTS `specials`;
CREATE TABLE IF NOT EXISTS `specials` (
  `id` tinyint(1) UNSIGNED DEFAULT '1',
  `specials` text,
  `leasing_terms` text,
  `pet_policy` text,
  `parking_policy` text,
  `show_specials` tinyint(1) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specials`
--

INSERT INTO `specials` (`id`, `specials`, `leasing_terms`, `pet_policy`, `parking_policy`, `show_specials`, `created_at`, `updated_at`) VALUES
(1, '<p>Aenean placerat commodo risus, ac sollicitudin urna volutpat et. Aenean augue sapien, efficitur a augue et, auctor viverra lacus. Phasellus feugiat ac purus at tincidunt. Cras luctus convallis ullamcorper. Nullam est nisl, sodales quis sem vitae, efficitur sollicitudin felis. Curabitur aliquam posuere risus in accumsan. Quisque ullamcorper ut sem ac pulvinar.</p>', '<p>Integer elementum est nisi, in tempor justo dapibus sit amet. Fusce aliquet magna dapibus nunc vehicula rutrum. Praesent consectetur pretium augue, ut sollicitudin libero bibendum et. Donec ac ornare neque. Vivamus arcu urna, volutpat in tortor vitae, ultricies efficitur elit.</p>', '<p>Vestibulum eu dolor volutpat, tempus neque sed, lacinia ante. Sed vitae eros tincidunt, venenatis felis vel, malesuada diam. Phasellus turpis metus, molestie non leo in, rutrum tempus mi.</p>', '<p>Donec convallis arcu ac ornare ultrices. Curabitur ac tincidunt purus, et aliquet purus. Aliquam consequat sapien quis fringilla pretium. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam tempus sollicitudin nibh eget hendrerit. In pharetra diam massa, ut dignissim ipsum sodales vitae. Sed vel finibus nibh.</p>', 1, '2019-05-17 14:38:25', '2019-06-11 05:25:54');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `units`;
CREATE TABLE IF NOT EXISTS `units` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `show` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `list_1st` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_available` date DEFAULT NULL,
  `rent` int(10) UNSIGNED DEFAULT NULL,
  `floor_plan_id` int(10) UNSIGNED DEFAULT NULL,
  `scheme_id` int(10) UNSIGNED DEFAULT NULL,
  `view_id` int(10) UNSIGNED DEFAULT NULL,
  `floor_no` int(2) UNSIGNED DEFAULT NULL,
  `sqft` int(4) UNSIGNED DEFAULT NULL,
  `feature_1` varchar(50) DEFAULT NULL,
  `feature_2` varchar(50) DEFAULT NULL,
  `feature_3` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=163 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `show`, `list_1st`, `date_available`, `rent`, `floor_plan_id`, `scheme_id`, `view_id`, `floor_no`, `sqft`, `feature_1`, `feature_2`, `feature_3`, `created_at`, `updated_at`) VALUES
(5, '313', 1, 0, '2019-05-29', 3000, 3, 3, 2, 3, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(4, '311', 1, 0, '2019-05-28', 3000, 4, 2, 2, 3, 829, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(3, '309', 1, 0, '2019-05-27', 3000, 3, 2, 2, 3, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(2, '307', 1, 0, '2019-05-26', 3000, 11, 2, 2, 3, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(1, '302', 1, 0, '2019-05-25', 3000, 3, 2, 6, 3, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(6, '315', 1, 0, '2019-05-30', 3000, 5, 3, 2, 3, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(7, '317', 1, 0, '2019-05-31', 3000, 3, 3, 2, 3, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(8, '320', 1, 0, '2019-06-01', 3000, 5, 3, 5, 3, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(9, '319', 1, 0, '2019-06-02', 3000, 10, 3, 1, 3, 1370, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(10, '321', 1, 0, '2019-06-03', 3000, 3, 3, 5, 3, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(11, '322', 1, 0, '2019-06-04', 3000, 3, 3, 6, 3, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(12, '323', 1, 0, '2019-06-05', 3000, 5, 3, 5, 3, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(13, '324', 1, 0, '2019-06-06', 3000, 5, 3, 6, 3, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(14, '325', 1, 0, '2019-06-07', 3000, 5, 1, 5, 3, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(15, '326', 1, 0, '2019-06-08', 3000, 5, 1, 6, 3, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(16, '327', 1, 0, '2019-06-09', 3000, 3, 1, 5, 3, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(17, '328', 1, 0, '2019-06-10', 3000, 3, 1, 6, 3, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(18, '330', 1, 0, '2019-06-11', 3000, 8, 1, 1, 3, 1350, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(19, '331', 1, 0, '2019-06-12', 3000, 9, 1, 4, 3, 1383, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(20, '332', 1, 0, '2019-06-13', 3000, 3, 1, 5, 3, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(21, '333', 1, 0, '2019-06-14', 3000, 5, 1, 4, 3, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(22, '335', 1, 0, '2019-06-15', 3000, 3, 1, 4, 3, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(23, '337', 1, 0, '2019-06-16', 3000, 5, 1, 4, 3, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(24, '339', 1, 0, '2019-06-17', 3000, 3, 1, 4, 3, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(25, '340', 1, 0, '2019-06-18', 3000, 3, 2, 6, 3, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(26, '341', 1, 0, '2019-06-19', 3000, 4, 2, 4, 3, 829, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(27, '343', 1, 0, '2019-06-20', 3000, 3, 2, 4, 3, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(28, '401', 1, 0, '2019-06-21', 3000, 5, 1, 3, 4, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(29, '402', 1, 0, '2019-06-22', 3000, 3, 1, 6, 4, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(30, '403', 1, 0, '2019-06-23', 3000, 1, 1, 3, 4, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(31, '407', 1, 0, '2019-06-24', 3000, 11, 1, 2, 4, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(32, '409', 1, 0, '2019-06-25', 3000, 3, 1, 2, 4, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(33, '411', 1, 0, '2019-06-26', 3000, 5, 1, 2, 4, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(34, '413', 1, 0, '2019-06-27', 3000, 3, 2, 2, 4, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(35, '415', 1, 0, '2019-06-28', 3000, 5, 2, 2, 4, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(36, '417', 1, 0, '2019-06-29', 3000, 3, 2, 2, 4, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(37, '420', 1, 0, '2019-06-30', 3000, 5, 2, 5, 4, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(38, '419', 1, 0, '2019-07-01', 3000, 10, 2, 1, 4, 1370, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(39, '421', 1, 0, '2019-07-02', 3000, 3, 2, 5, 4, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(40, '422', 1, 0, '2019-07-03', 3000, 3, 2, 6, 4, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(41, '423', 1, 0, '2019-07-04', 3000, 5, 2, 5, 4, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(42, '424', 1, 0, '2019-07-05', 3000, 5, 2, 6, 4, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(43, '425', 1, 0, '2019-07-06', 3000, 5, 3, 5, 4, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(44, '426', 1, 0, '2019-07-07', 3000, 5, 3, 6, 4, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(45, '427', 1, 0, '2019-07-08', 3000, 3, 3, 5, 4, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(46, '428', 1, 0, '2019-07-09', 3000, 3, 3, 6, 4, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(47, '430', 1, 0, '2019-07-10', 3000, 8, 3, 1, 4, 1350, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(48, '431', 1, 0, '2019-07-11', 3000, 9, 3, 1, 4, 1383, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(49, '432', 1, 0, '2019-07-12', 3000, 3, 3, 5, 4, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(50, '433', 1, 0, '2019-07-13', 3000, 5, 3, 4, 4, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(51, '434', 1, 0, '2019-07-14', 3000, 1, 3, 5, 4, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(52, '435', 1, 0, '2019-07-15', 3000, 3, 3, 4, 4, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(53, '437', 1, 0, '2019-07-16', 3000, 5, 3, 4, 4, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(54, '439', 1, 0, '2019-07-17', 3000, 3, 3, 4, 4, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(55, '440', 1, 0, '2019-07-18', 3000, 3, 1, 6, 4, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(56, '441', 1, 0, '2019-07-19', 3000, 5, 1, 4, 4, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(57, '442', 1, 0, '2019-07-20', 3000, 2, 1, 3, 4, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(58, '443', 1, 0, '2019-07-21', 3000, 3, 1, 4, 4, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(59, '444', 1, 0, '2019-07-22', 3000, 5, 1, 3, 4, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(60, '445', 1, 0, '2019-07-23', 3000, 6, 1, 4, 4, 1194, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(61, '501', 1, 0, '2019-07-24', 3000, 5, 1, 3, 5, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(62, '502', 1, 0, '2019-07-25', 3000, 3, 1, 6, 5, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(63, '503', 1, 0, '2019-07-26', 3000, 1, 1, 3, 5, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(64, '507', 1, 0, '2019-07-27', 3000, 11, 1, 2, 5, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(65, '509', 1, 0, '2019-07-28', 3000, 3, 1, 2, 5, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(66, '511', 1, 0, '2019-07-29', 3000, 5, 1, 2, 5, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(67, '513', 1, 0, '2019-07-30', 3000, 3, 3, 2, 5, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(68, '515', 1, 0, '2019-07-31', 3000, 5, 3, 2, 5, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(69, '517', 1, 0, '2019-08-01', 3000, 3, 3, 2, 5, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(70, '518', 1, 0, '2019-08-02', 3000, 7, 3, 1, 5, 1230, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(71, '519', 1, 0, '2019-08-03', 3000, 10, 3, 1, 5, 1370, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(72, '520', 1, 0, '2019-08-04', 3000, 5, 3, 5, 5, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(73, '521', 1, 0, '2019-08-05', 3000, 3, 3, 5, 5, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(74, '522', 1, 0, '2019-08-06', 3000, 3, 3, 6, 5, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(75, '523', 1, 0, '2019-08-07', 3000, 5, 3, 5, 5, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(76, '524', 1, 0, '2019-08-08', 3000, 5, 3, 6, 5, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(77, '525', 1, 0, '2019-08-09', 3000, 5, 2, 5, 5, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(78, '526', 1, 0, '2019-08-10', 3000, 5, 2, 6, 5, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(79, '527', 1, 0, '2019-08-11', 3000, 3, 2, 5, 5, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(80, '528', 1, 0, '2019-08-12', 3000, 3, 2, 6, 5, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(81, '530', 1, 0, '2019-08-13', 3000, 8, 2, 1, 5, 1350, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(82, '531', 1, 0, '2019-08-14', 3000, 9, 2, 1, 5, 1383, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(83, '532', 1, 0, '2019-08-15', 3000, 3, 2, 5, 5, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(84, '533', 1, 0, '2019-08-16', 3000, 5, 2, 4, 5, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(85, '534', 1, 0, '2019-08-17', 3000, 1, 2, 5, 5, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(86, '535', 1, 0, '2019-08-18', 3000, 3, 2, 4, 5, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(87, '537', 1, 0, '2019-08-19', 3000, 5, 2, 4, 5, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(88, '539', 1, 0, '2019-08-20', 3000, 3, 2, 4, 5, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(89, '540', 1, 0, '2019-08-21', 3000, 3, 1, 6, 5, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(90, '541', 1, 0, '2019-08-22', 3000, 5, 1, 4, 5, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(91, '542', 1, 0, '2019-08-23', 3000, 2, 1, 3, 5, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(92, '543', 1, 0, '2019-08-24', 3000, 3, 1, 4, 5, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(93, '544', 1, 0, '2019-08-25', 3000, 5, 1, 3, 5, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(94, '545', 1, 0, '2019-08-26', 3000, 6, 1, 4, 5, 1194, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(95, '601', 1, 0, '2019-08-27', 3000, 5, 2, 3, 6, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(96, '602', 1, 0, '2019-08-28', 3000, 3, 2, 6, 6, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(97, '603', 1, 0, '2019-08-29', 3000, 1, 2, 3, 6, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(98, '607', 1, 0, '2019-08-30', 3000, 11, 2, 2, 6, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(99, '609', 1, 0, '2019-08-31', 3000, 3, 2, 2, 6, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(100, '611', 1, 0, '2019-09-01', 3000, 5, 2, 2, 6, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(101, '613', 1, 0, '2019-09-02', 3000, 3, 1, 2, 6, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(102, '615', 1, 0, '2019-09-03', 3000, 5, 1, 2, 6, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(103, '617', 1, 0, '2019-09-04', 3000, 3, 1, 2, 6, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(104, '618', 1, 0, '2019-09-05', 3000, 7, 1, 1, 6, 1230, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(105, '619', 1, 0, '2019-09-06', 3000, 10, 1, 1, 6, 1370, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(106, '620', 1, 0, '2019-09-07', 3000, 5, 1, 5, 6, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(107, '621', 1, 0, '2019-09-08', 3000, 3, 1, 5, 6, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(108, '622', 1, 0, '2019-09-09', 3000, 3, 1, 6, 6, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(109, '623', 1, 0, '2019-09-10', 3000, 5, 1, 5, 6, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(110, '624', 1, 0, '2019-09-11', 3000, 5, 1, 6, 6, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(111, '625', 1, 0, '2019-09-12', 3000, 5, 3, 5, 6, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(112, '626', 1, 0, '2019-09-13', 3000, 5, 3, 6, 6, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(113, '627', 1, 0, '2019-09-14', 3000, 3, 3, 5, 6, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(114, '628', 1, 0, '2019-09-15', 3000, 3, 3, 6, 6, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(115, '630', 1, 0, '2019-09-16', 3000, 8, 3, 1, 6, 1350, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(116, '631', 1, 0, '2019-09-17', 3000, 9, 3, 1, 6, 1383, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(117, '632', 1, 0, '2019-09-18', 3000, 3, 3, 5, 6, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(118, '633', 1, 0, '2019-09-19', 3000, 5, 3, 4, 6, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(119, '634', 1, 0, '2019-09-20', 3000, 1, 3, 5, 6, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(120, '635', 1, 0, '2019-09-21', 3000, 3, 3, 4, 6, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(121, '637', 1, 0, '2019-09-22', 3000, 5, 3, 4, 6, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(122, '639', 1, 0, '2019-09-23', 3000, 3, 3, 4, 6, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(123, '640', 1, 0, '2019-09-24', 3000, 3, 2, 6, 6, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(124, '641', 1, 0, '2019-09-25', 3000, 5, 2, 4, 6, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(125, '642', 1, 0, '2019-09-26', 3000, 2, 2, 3, 6, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(126, '643', 1, 0, '2019-09-27', 3000, 3, 2, 4, 6, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(127, '644', 1, 0, '2019-09-28', 3000, 5, 2, 3, 6, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(128, '645', 1, 0, '2019-09-29', 3000, 6, 2, 4, 6, 1194, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(129, '701', 1, 0, '2019-09-30', 3000, 5, 3, 3, 7, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(130, '702', 1, 0, '2019-10-01', 3000, 3, 3, 6, 7, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(131, '703', 1, 0, '2019-10-02', 3000, 1, 3, 3, 7, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(132, '707', 1, 0, '2019-10-03', 3000, 11, 3, 2, 7, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(133, '709', 1, 0, '2019-10-04', 3000, 3, 3, 2, 7, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(134, '711', 1, 0, '2019-10-05', 3000, 5, 3, 2, 7, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(135, '713', 1, 0, '2019-10-06', 3000, 3, 2, 2, 7, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(136, '715', 1, 0, '2019-10-07', 3000, 5, 2, 2, 7, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(137, '717', 1, 0, '2019-10-08', 3000, 3, 2, 2, 7, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(138, '718', 1, 0, '2019-10-09', 3000, 7, 2, 1, 7, 1239, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(139, '719', 1, 0, '2019-10-10', 3000, 10, 2, 1, 7, 1370, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(140, '720', 1, 0, '2019-10-11', 3000, 5, 2, 5, 7, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(141, '721', 1, 0, '2019-10-12', 3000, 3, 2, 5, 7, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(142, '722', 1, 0, '2019-10-13', 3000, 3, 2, 6, 7, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(143, '723', 1, 0, '2019-10-14', 3000, 5, 2, 5, 7, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(144, '724', 1, 0, '2019-10-15', 3000, 5, 2, 6, 7, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(145, '725', 1, 0, '2019-10-16', 3000, 5, 1, 5, 7, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(146, '726', 1, 0, '2019-10-17', 3000, 5, 1, 6, 7, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(147, '727', 1, 0, '2019-10-18', 3000, 3, 1, 5, 7, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(148, '728', 1, 0, '2019-10-19', 3000, 3, 1, 6, 7, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(149, '730', 1, 0, '2019-10-20', 3000, 8, 1, 1, 7, 1350, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(150, '731', 1, 0, '2019-10-21', 3000, 9, 1, 1, 7, 1383, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(151, '732', 1, 0, '2019-10-22', 3000, 3, 1, 5, 7, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(152, '733', 1, 0, '2019-10-23', 3000, 5, 1, 4, 7, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(153, '734', 1, 0, '2019-10-24', 3000, 1, 1, 5, 7, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(154, '735', 1, 0, '2019-10-25', 3000, 3, 1, 4, 7, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(155, '737', 1, 0, '2019-10-26', 3000, 5, 1, 4, 7, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(156, '739', 1, 0, '2019-10-27', 3000, 3, 1, 4, 7, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(157, '740', 1, 0, '2019-10-28', 3000, 3, 3, 6, 7, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(158, '741', 1, 0, '2019-10-29', 3000, 5, 3, 4, 7, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(159, '742', 1, 0, '2019-10-30', 3000, 2, 3, 3, 7, 1687, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(160, '743', 1, 0, '2019-10-31', 3000, 3, 3, 4, 7, 835, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(161, '744', 1, 0, '2019-11-01', 3000, 5, 3, 3, 7, 1136, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00'),
(162, '745', 1, 0, '2019-11-02', 3000, 6, 3, 4, 7, 1194, 'a', 'a', 'a', '2019-05-20 18:30:00', '2019-05-21 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receive_emails` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_password_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `password`, `receive_emails`, `remember_token`, `new_password_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Administrator', 'admin@vespaio.com', NULL, '$2y$10$7DX2mqDI7Fxi0pTJGGa/KOJcttAdQOJmuHXBBxqmfH/44Q4I83xe6', 1, 'BVMcpvzQbHyjyutEEQPk3bXkKOeBsg5iN43ACQFKIMsjjxB37CZ6EurkVWph', NULL, '2019-05-10 09:18:54', '2019-05-31 06:45:15'),
(2, 2, 'Steve Vella', 'steve.vella@riverwalkapts.com', NULL, NULL, 1, NULL, NULL, '2019-05-13 05:42:09', '2019-05-14 07:30:07'),
(29, 2, 'Vikas Pandey', 'vikas@ssquares.co.in', NULL, '$2y$10$cwmqufSmvWkFyEQZbZSe..xAHZFbVzSAkEkfPD1nV6CxPwByNAnyS', 1, NULL, NULL, '2019-05-31 06:46:39', '2019-05-31 06:47:52');

-- --------------------------------------------------------

--
-- Table structure for table `views`
--

DROP TABLE IF EXISTS `views`;
CREATE TABLE IF NOT EXISTS `views` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `views`
--

INSERT INTO `views` (`id`, `name`) VALUES
(1, 'NE - Downtown'),
(2, 'SE - Mountains'),
(3, 'SW - Mountains'),
(4, 'NW - Valley'),
(5, 'Pool'),
(6, 'Courtyard');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
