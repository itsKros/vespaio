$(document).ready(function () {
    $('#mainheader').next().css({'min-height':'59vh'});
    
    if ($(window).width() < 767) {
        $('table').addClass('table-responsive');
    }

    $('.bs-confirm-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        url = button.attr('action'); // Extract info from data-* attributes
        console.log(url);
        $('#delete-confirm').click(function(){
            window.location.href = url;
        })
    });

});
