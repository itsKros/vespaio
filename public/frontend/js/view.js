var viewer = null;
var panorama = null;

function createView($view, $lat, $lng, $pano, $zoom, $pov){
    if(panorama) panorama = null;
    if(viewer) viewer.destroy();
    panorama = new google.maps.StreetViewPanorama(
        document.getElementById($view),
        {
            position: {lat: $lat, lng: $lng},
            pov: $pov,
            zoom: $zoom,
            pano: $pano,
            panControl:false,
            zoomControl:false,
            addressControl:false,
            disableDefaultUI:true,
            clickToGo: true,
            visible: true,
            linksControl: true,
        });
    setTimeout(function(){
        $('.gm-style > div:nth-child(9)').remove();
    },1000);
}

function createPano(fileName, auto=false){
    $director = `<img src="/frontend/img/director.png"/>`;
    $hotspot = `<img src="/frontend/img/hotspot.png"/>`;
    $pano = "/frontend/img/panos/"+fileName+".jpg";

    if(panorama) panorama = null;
    if(viewer) viewer.destroy();
    viewer = pannellum.viewer('panorama', {
        "type": "equirectangular",
        "panorama": $pano,
        "autoLoad": true,
        "pitch": 0,
        "yaw": (fileName === 'B1-1')? 0 : 0,
        "hfov":120,
        "showControls": false
    });

    if(auto){
        viewer.on('animatefinished', function(){
            $('#rect-6-5 img').css('transform', 'rotate('+viewer.getYaw()+'deg)');
        });
    }

    if(fileName === 'B1-1'){
        $('#rect-4-7').empty();
        $('#rect-6-5').empty();
        $('#rect-6-5').append($director);
        $('#rect-6-5 img').css('transform', 'rotate(0deg)');
        $('#rect-4-7').append($hotspot);
        $('#rect-4-7').css('cursor','pointer');

        $('#rect-4-7').unbind('click').bind('click', function(){
            createPano('B1-2');
            $('#rect-6-5').css('cursor','pointer');
            $('#rect-4-7').empty();
            $('#rect-4-7').append($director);
            $('#rect-4-7 img').css('transform', 'rotate(120deg)');
            $('#rect-6-5').empty()
            $('#rect-6-5').append($hotspot);
            $('#rect-6-5').css('cursor','pointer');
            viewer.on('animatefinished', function(){
                $('#rect-4-7 img').css('transform', 'rotate('+(120+viewer.getYaw())+'deg)');
            });
        });

        $('#rect-6-5').unbind('click').bind('click', function(){
            createPano('B1-1');
            $('#rect-4-7').css('cursor','pointer');
            $('#rect-6-5').empty();
            $('#rect-6-5').append($director);
            $('#rect-6-5 img').css('transform', 'rotate(120deg)');
            $('#rect-4-7').empty()
            $('#rect-4-7').append($hotspot);
            $('#rect-4-7').css('cursor','pointer');
            viewer.on('animatefinished', function(){
                $('#rect-6-5 img').css('transform', 'rotate('+viewer.getYaw()+'deg)');
            });
        });
    }
}

function initialize(){

}

