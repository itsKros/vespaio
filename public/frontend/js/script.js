$(function(){
    $(".matchsearch .content table.list").niceScroll('.matchsearch .content table.list tbody',
    {
        cursorcolor:"rgba(0,0,0,0.5)",
        cursorwidth:"4px",
        cursorborder:"0px",
        autohidemode:false
    });
    $('.matchsearch .content table.list tbody').getNiceScroll().onResize();
    
    $('#sizeMin, #sizeMid, #sizeMax').click(function(){
        $('.matchsearch .content table.list tbody').getNiceScroll().onResize();
    });


    
    $(".unithighlight").niceScroll(
    {
        cursorcolor:"rgba(0,0,0,0.5)",
        cursorwidth:"4px",
        cursorborder:"0px",
        autohidemode:false,
        horizrailenabled:false
    });
    $('.unithighlight').getNiceScroll().onResize();
    
    $(".matchdetail").niceScroll(
    {
        cursorcolor:"rgba(0,0,0,0.5)",
        cursorwidth:"4px",
        cursorborder:"0px",
        autohidemode:false,
        horizrailenabled:false
    });
    $('.matchdetail').getNiceScroll().onResize();

    $(".matchsearch .unithighlight .content textarea").niceScroll( 
    {
        cursorcolor:"rgba(0,0,0,0.5)",
        cursorwidth:"4px",
        cursorborder:"0px",
        autohidemode:true
    });
  
    
    $('.selectunit .units').click(function(){
        setTimeout(function(){
            $('.matchsearch .unithighlight .content textarea').getNiceScroll().resize()
          }, 500);
       
    });

});

$(document).ready(function(){
    /**Unit Scroller Owl Carousel */
    $(".unitscroller").owlCarousel({
        margin:5,
        items:10,
        loop:false,
        autoWidth:true,
        dots:false,
    });

    // /* Mobile filter units Owl Carousel */
    // let mowl = $(".m .filterunit .unitslide");
    // mowl.owlCarousel({
    //     margin:14,
    //     items:4,
    //     loop:false,
    //     autoWidth:false,
    //     dots:false,
    //     nav: true,
    //     navText: ["<div class='arrow left'></div>","<div class='arrow right'></div>"]

    // });

      
        
});


