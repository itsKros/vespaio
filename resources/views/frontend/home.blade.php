@extends('layouts.frontend.master')
@section('content')
    <div class="container main" id="home" v-cloak>
            
        <!--Desktop Start-->
        <div class="row desktop" style="z-index:100;">
            <div class="col-md-9 fh">
                @include('layouts.frontend.part.selectunit')
                @include('layouts.frontend.part.selectplan')
                @include('layouts.frontend.part.searchfilter')
            </div>
            <div class="col-md-3 sh">
                @include('layouts.frontend.part.matchsearch')
            </div>
        </div>
        <!--Desktop Ends-->

        <!--Mobile Start-->
        <div class="row mobile">
            <div class="col-sm-12 m">
               @include('layouts.frontend.part.plantypemobile')
            </div>
        </div>
        <!--Mobile Ends-->
    </div>

    <div id="view"></div>

    <div id="panorama"></div>
@endsection
@section('js')
    <script>

        let app = new Vue({

            el:'#home',
            data:{
                units : {!! json_encode($units) !!},
                floorPlans : {!! json_encode($floor_plans) !!},
                views : {!! json_encode($views) !!},
                schemes : {!! json_encode($schemes) !!},
                floors : {!! json_encode($floors) !!},
                periods : {!! json_encode($periods) !!},
                rentRanges : {!! json_encode($rent_ranges) !!},
                sortIconBlue : {!! json_encode(asset('frontend/img/sort-blue.png')) !!},
                sortIconGrey : {!! json_encode(asset('frontend/img/sort-grey.png')) !!},
                assetsUrl : {!! json_encode(asset('frontend/')) !!},
                activeUnit : null,
                detailedUnit : null,
                listClass : 'medium',
                detailClass : 'max',
                activePlan : 0,
                sortOrder : {by:'name', order:1},
                
                filterBtnVal: 'open',
                isFloorPlan : false,
                
                isFloorHidden: true,
                isViewHidden: true,
                isSchemeHidden: true,
                isAvailableHidden: true,
                isRentHidden: true,
                
                iIcon:false,
                isInfoItems:false,
                isInfoData: false,
                isSpecial:false,
                isPet:false,
                isLeasing:false,
                isParking:false,

                /**Mobile Data */
                isListWrap:true,
                isMatchListWrap:true,
                isFilterWrap:true,
                isInfoWrap:true,
                newLineStr:'\n',
                bulletStr:'\u2022 '
            },

            watch: {
                filteredUnits: function(){
                    let self = this;
                    setTimeout(function(){
                        self.setSitePlan();
                        self.setUnitView();
                    }, 500);
                }
            },

            computed: {
                unitAvailability() {
                    let dateAvailable = new Date(this.detailedUnit.date_available);
                    let today = new Date();
                    let diffTime = dateAvailable.getTime() - today.getTime();
                    let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                    if (diffDays <= 0) {
                        return 'Available Now'
                    } else {
                        return this.detailedUnit.date_available
                    }
                },
                filteredUnits: {

                    get: function () {
                        let self = this;
                        let filteredUnits = self.units;

                        // filter through floor plans
                        filteredUnits = self.units.filter(function (unit) {
                            if (self.floorPlans[0].checked) return filteredUnits;
                            return self.floorPlans.find(function (plan) {
                                return plan.checked && (unit.floor_plan_id === plan.id);
                            });
                        });

                        // Filter through floor no.
                        filteredUnits = filteredUnits.filter(function (unit) {
                            if (self.floors[0].checked) return filteredUnits;
                            return self.floors.find(function (floor) {
                                return floor.checked && (unit.floor_no === floor.id);
                            });
                        });

                        // Filter through views
                        filteredUnits = filteredUnits.filter(function (unit) {
                            if (self.views[0].checked) return filteredUnits;
                            return self.views.find(function (view) {
                                return view.checked && (unit.view_id === view.id);
                            });
                        });

                        // Filter through schemes
                        filteredUnits = filteredUnits.filter(function (unit) {
                            if (self.schemes[0].checked) return filteredUnits;
                            return self.schemes.find(function (scheme) {
                                return scheme.checked && (unit.scheme_id === scheme.id);
                            });
                        });

                        // Filter through periods
                        filteredUnits = filteredUnits.filter(function (unit) {
                            if (self.periods[0].checked) return filteredUnits;
                            return self.periods.find(function (period) {
                                if (period.checked) {
                                    let dateAvailable = new Date(unit.date_available);
                                    let today = new Date();
                                    let diffTime = dateAvailable.getTime() - today.getTime();
                                    let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                                    if (period.name === 'Now') {
                                        return diffDays < 1;
                                    } else {
                                        let days = period.id.replace('Days', '').split('-');
                                        if (days[1] == 0) {
                                            return diffDays >= parseInt(days[0]);
                                        }
                                        return diffDays >= parseInt(days[0]) && diffDays <= parseInt(days[1]);
                                    }
                                }
                            });
                        });

                        // Filter through rent ranges
                        filteredUnits = filteredUnits.filter(function (unit) {
                            if (self.rentRanges[0].checked) return filteredUnits;
                            return self.rentRanges.find(function (rentRange) {
                                let ranges = rentRange.id.split('-');
                                if (ranges[1] == 0) return rentRange.checked && unit.rent >= ranges[0];
                                return rentRange.checked && unit.rent >= ranges[0] && unit.rent <= ranges[1];
                            });
                        });

                        filteredUnits = filteredUnits.sort((a, b) => {
                            let modifier = 1;
                            if (this.sortOrder.order === 0) modifier = -1;
                            if (this.sortOrder.by === 'scheme' || this.sortOrder.by === 'floor_plan') {
                                if (a[this.sortOrder.by].name < b[this.sortOrder.by].name) return -1 * modifier;
                                if (a[this.sortOrder.by].name > b[this.sortOrder.by].name) return 1 * modifier;
                                return 0;
                            }
                            if (a[this.sortOrder.by] < b[this.sortOrder.by]) return -1 * modifier;
                            if (a[this.sortOrder.by] > b[this.sortOrder.by]) return 1 * modifier;
                            return 0;
                        });

                        this.activeUnit = this.detailedUnit = filteredUnits.length > 0 ? filteredUnits[0] : null;

                        $(".m .filterunit .unitslide").trigger('destroy.owl.carousel');
                        Vue.nextTick(function(){
                            self.initOwlCaraousel();
                        });

                        return filteredUnits;
                    },

                    set: function (newValue) {
                        return newValue;
                    }
                },
            },
            methods:{
                initOwlCaraousel(){
                    $(".m .filterunit .unitslide").owlCarousel({
                        margin:10,
                        item:4,
                        loop:false,
                        autoWidth:false,
                        dots:false,
                        nav: true,
                        navText: ["<div class='arrow left'></div>","<div class='arrow right'></div>"]
                    });
                },
                unitClicked(unit){
                    if(this.activeUnit.floor_no == unit.floor_no){
                        this.activeUnit = this.detailedUnit = unit;
                        this.setSitePlanUnitActive();
                    }else{
                        this.activeUnit = this.detailedUnit = unit;
                        this.setSitePlan();
                    }
                    this.setUnitView();
                },
                setUnitView(){
                    if(this.activeUnit.floor_plan.name === 'B1'){
                        createPano('B1-1', true);
                    }else{
                        createView('view', 37.296851, -121.9305717, 'a2q9WXmUwK8AAAQvvIf-mA', 0, {heading: 221.13, pitch: -12});
                    }
                },
                setSitePlan(){
                    if(this.activeUnit == null) return;
                    let self = this;
                    fetch(this.assetsUrl+'/img/site_plans/'+this.activeUnit.floor_no+'.svg')
                        .then(response => response.text())
                        .then((data) => {
                            $('.floordetail .floor').fadeOut('fast');
                            $('.floordetail .floor').empty();
                            $src = this.assetsUrl+'/img/site_plans/'+this.activeUnit.floor_no+'.jpg';
                            $img = '<img src="'+$src+'"/>';
                            $('.floordetail .floor').append($img);
                            $('.floordetail .floor').append(data.toString());
                            $('.floordetail .floor').fadeIn('slow');
                            this.filteredUnits.map(function(unit, index){
                                $('.'+unit.name).addClass('highlighted');
                                $('.'+unit.name).hover(
                                    ()=>{
                                        self.detailedUnit = unit;
                                        $('rect'+'.'+unit.name).css('fill', '#92d050');
                                    },
                                    ()=>{
                                        self.detailedUnit = self.activeUnit;
                                        $('rect'+'.'+unit.name).css('fill', '#79ADF6');
                                    }
                                    );
                                $('.'+unit.name).click(()=>{self.unitClicked(unit)});
                            });
                            this.setSitePlanUnitActive();
                        });
                },
                setSitePlanUnitActive(){
                    this.filteredUnits.map(function(unit, index){
                        $('.'+unit.name).removeClass('active');
                    });
                    $('.'+this.activeUnit.name).addClass('active');
                },
                filterClicked(item, data){
                    if(item.id == 0 && !item.checked){
                        data = data.map(function(item, index){
                            if(index != 0) item.checked = false;
                            return item;
                        })
                    }else if(item.id != 0 && !item.checked){
                        data[0].checked = false;
                    }
                },
                getCheckedFilters(filterable){
                    let filterStr = 'All';
                    for(let i=1; i<filterable.length; i++){
                        if(filterable[i].checked){
                            if(filterStr === 'All')
                                filterStr = filterable[i].name;
                            else
                                filterStr = filterStr + ((filterStr.indexOf('+')===-1)? '+' : '');
                        }
                    }
                    return filterStr;
                },
                sortList(column){
                    this.sortOrder.by = column;
                    this.sortOrder.order = (this.sortOrder.order===1)? 0 : 1;
                },
                getSortRotation(column){
                    return (this.sortOrder.by===column && this.sortOrder.order === 0)? 'transform : rotate(180deg)': '';
                },
                getSortIcon(column){
                    return (this.sortOrder.by === column)? this.sortIconBlue : this.sortIconGrey;
                },
                getAvailabilityStr(date){
                    let dateAvailable = new Date(date);
                    let today = new Date();
                    let diffTime = dateAvailable.getTime() - today.getTime();
                    let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                    if(diffDays<=0){
                        return 'Avail. Now'
                    }else{
                        return date
                    }
                },
                getAvailableUnitsInFloor(floor_no){
                    let self = this;
                    let availableUnits = this.units.filter(function(unit){
                        return unit.floor_no === floor_no && unit.date_available !== null;
                    });
                    return availableUnits.length;
                },
                floorClicked(floor){
                    let filteredUnitsByFloor = this.filteredUnits.filter(function(unit){
                        return unit.floor_no == floor.id;
                    });
                    this.activeUnit = this.detailedUnit = (filteredUnitsByFloor.length>0)? filteredUnitsByFloor[0] : null;
                    this.setSitePlan();
                },
                getFilterString(){
                    // Plans<span> A1+A2 </span>| Floor<span> 3 </span>| View <span> All </span>| Color Scheme<span> All </span>| Rent<span> All </span>
                    let filters = ' <span>SHOWING </span>| Plans <span>';
                    let names = [];
                    let floor_no = [];
                    let views = [];
                    let color_schemes = [];
                    let rents = [];

                    this.floorPlans.forEach(function(plan) {
                        if (plan.checked) {
                            if(plan.id === 0) names.push(['All']);
                            else names.push(plan.name);
                        }
                    });

                    filters += names.join('+') + '</span> | Floor <span>';

                    this.floors.forEach(function(floor){
                        if(floor.checked){
                            if(floor.id === 0) floor_no.push(['All']);
                            else floor_no.push(floor.id);
                        }
                    });

                    filters +=  floor_no.join('+') + '</span> | View <span>';

                    this.views.forEach(function(view){
                        if(view.checked){
                            if(view.id === 0) views.push(['All']);
                            else views.push(view.name);
                        }
                    });

                    filters +=  views.join('+') + '</span> | Color Scheme <span>';

                    this.schemes.forEach(function(scheme){
                        if(scheme.checked){
                            if(scheme.id === 0) color_schemes.push(['All']);
                            else color_schemes.push(scheme.name);
                        }
                    });

                    filters += color_schemes.join('+')  + '</span> | Rent <span>';

                    this.rentRanges.forEach(function(range){
                        if(range.checked){
                            if(range.id === 0) rents.push(['All']);
                            else rents.push(range.name);
                        }
                    });

                    filters +=  rents.join('+') + '</span>';

                    return filters;
                },

                getBulletedFeatures: function(features){
                    let featuresArr = features.split('\n');
                    let self = this;
                    let featuresStr = featuresArr.reduce(function(acc, feature){
                        return  acc + self.bulletStr + feature + self.newLineStr;
                    }, '');
                    console.log(featuresStr);
                    return featuresStr;
                },

                initiateSelect: function(){ 
                    this.isFilterWrap = !this.isFilterWrap;
                    $('.js-example-basic-single').select2({
                        minimumResultsForSearch: Infinity
                    });
                },
                
                filterBtn: function(){
                    if(this.filterBtnVal == 'open'){
                        this.isFloorHidden = false;
                        this.isViewHidden = false;
                        this.isSchemeHidden = false;
                        this.isAvailableHidden = false;
                        this.isRentHidden = false;
                        this.filterBtnVal = "close";
                    } else {
                        this.isFloorHidden = true;
                        this.isViewHidden = true;
                        this.isSchemeHidden = true;
                        this.isAvailableHidden = true;
                        this.isRentHidden = true;
                        this.filterBtnVal = "open";
                    }
                },
                infoData: function(){
                    this.isInfoItems = false;   /** Info Buttons Wrapper (Special, Pet, etc. Button) Hide */
                    this.isInfoData = false;    /** Info Data Wrapper (Special, Pet, etc. data) Hide */
                    this.isSpecial = false;     /** Special Data hide */
                    this.isPet = false;         /** Pet Data hide */
                    this.isLeasing = false;     /** Leasing Data hide */
                    this.isParking = false;     /** Parking Data hide */
                    this.iIcon = false;         /** i Icon Color Grey */
                },
                specialBtn: function(){
                    this.isSpecial = !this.isSpecial;   /** Special Data toggle */
                    this.isPet = false;                 /** Pet Data hide */
                    this.isLeasing = false;             /** Leasing Data hide */
                    this.isParking = false;             /** Parking Data hide */
                    this.isInfoData = true;             /** Info Data Wrapper (Special, Pet, etc. data) Show */
                    if(this.isSpecial == false){        
                        /** For Close Button */
                        this.isInfoItems = false;   /** Info Buttons Wrapper (Special, Pet, etc. Button) Hide */
                        this.isInfoData = false;    /** Info Data Wrapper (Special, Pet, etc. data) Hide */
                        this.isPet = false;         /** Pet Data hide */
                        this.isLeasing = false;     /** Leasing Data hide */
                        this.isParking = false;     /** Parking Data hide */
                        this.iIcon = false;         /** i Icon Color Grey */
                    }
                },
                petBtn: function(){
                    this.isSpecial = false;
                    this.isPet = !this.isPet;
                    this.isLeasing = false;
                    this.isParking = false;
                    this.isInfoData = true;
                    if(this.isPet == false){
                        this.isInfoItems = false;   /** Info Buttons Wrapper (Special, Pet, etc. Button) Hide */
                        this.isInfoData = false;    /** Info Data Wrapper (Special, Pet, etc. data) Hide */
                        this.isSpecial = false;     /** Special Data hide */
                        this.isLeasing = false;     /** Leasing Data hide */
                        this.isParking = false;     /** Parking Data hide */
                        this.iIcon = false;         /** i Icon Color Grey */
                    }
                },
                leasingBtn: function(){
                    this.isSpecial = false;
                    this.isPet = false;
                    this.isLeasing = !this.isLeasing;
                    this.isParking = false;
                    this.isInfoData = true;
                    if(this.isLeasing == false){
                        
                        this.isInfoItems = false;   /** Info Buttons Wrapper (Special, Pet, etc. Button) Hide */
                        this.isInfoData = false;    /** Info Data Wrapper (Special, Pet, etc. data) Hide */
                        this.isSpecial = false;     /** Special Data hide */
                        this.isPet = false;         /** Pet Data hide */
                        this.isParking = false;     /** Parking Data hide */
                        this.iIcon = false;         /** i Icon Color Grey */
                    }
                   
                },
                parkingBtn: function(){
                    this.isSpecial = false;
                    this.isPet = false;
                    this.isLeasing = false;
                    this.isParking = !this.isParking;
                    this.isInfoData = true;
                    if(this.isParking == false){
                        this.isInfoItems = false;   /** Info Buttons Wrapper (Special, Pet, etc. Button) Hide */
                        this.isInfoData = false;    /** Info Data Wrapper (Special, Pet, etc. data) Hide */
                        this.isSpecial = false;     /** Special Data hide */
                        this.isPet = false;         /** Pet Data hide */
                        this.isLeasing = false;     /** Leasing Data hide */
                        this.iIcon = false;         /** i Icon Color Grey */
                    }
                },
                infoIcon: function() {
                    this.isInfoItems = !this.isInfoItems;
                    this.isSpecial = !this.isSpecial;
                    this.isInfoData = !this.isInfoData;
                    this.iIcon = !this.iIcon;
                    this.isFloorPlan = false;
                },
                mapIcon: function(){
                    this.isFloorPlan = !this.isFloorPlan;
                    this.isInfoItems = false;
                    this.isInfoData = false;
                    this.isSpecial = false
                    this.iIcon = false;
                },
            },
            mounted(){
                this.setUnitView();
                this.setSitePlan();
            }
        });
    </script>
@endsection
