@extends('layouts.backend.master')

@section('css')
<style>
    section{height:100vh;display: flex;align-items: center;}
    ul.navbar-nav.ml-auto, 
    footer.footer{ display: none;}
    nav.navbar.navbar-expand-lg.navbar-light {display: none;}
    @media only screen and (max-width:767px){
        #logSec .login-form .form-group label {width: 37% !important;justify-content: flex-start !important;text-align: left !important;}
        #logSec .login-form .form-group input {width: 60%;margin-bottom: 8px;}
    }
</style>

@endsection

@section('content')


<section id="logSec">
    <div class="container">
            @include('layouts.backend.messages')
        <div class="row">
            <div class="col-md-3 logo"><img class="img-fluid" src="{{asset('backend/images/vespaio-admin-logo.png')}}"></div>
            <div class="col-md-9 login-form">
                <h5>Vespaio Website Dashboard Login</h5>
                <p>Use the Leasing Dashboard to easily update the information on the Vespaio website, including lease rates, availability, lease term, and more.</p>
                    <form method="POST" action="{{ route('login') }}" class="form-horizontal">
                        @csrf
                        <div class="form-group form-inline">
                            <input type="hidden" id="screen_height" name="screen_height"/>
                            <label for="email">Email address</label>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            <label for="password">Password</label>
                            <input id="password" type="password" class="form-control" name="password" required autocomplete="current-password">
                        </div>
                        
                        <input type="submit" class="btn btn-md btn-outline-primary" value="{{ __('Login') }}">
                            @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                        
                    </form>
            </div>
        </div>
    </div>    
</section>
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script>

        $screenHeight = window.innerHeight;
        $('#screen_height').val($screenHeight);
        Cookies.set('screenHeight', $screenHeight);

    </script>
@endsection
