@extends('layouts.backend.master')

@section('css')
<style>
    section{height:100vh;display: flex;align-items: center;}
    ul.navbar-nav.ml-auto, 
    footer.footer{ display: none;}
    #logSec .login-form form {margin:40px auto;}
    #logSec .login-form .form-group label {width: 20%;color: #fff;text-align: right;display: flex;justify-content: flex-end;margin-right: 10px;}
    #logSec .login-form .form-group input {width: 70%;padding: .25rem .75rem;height: 30px;border-radius: 0;border: 0;margin-bottom:5px}
</style>

@endsection

@section('content')


<section id="logSec">
        <div class="container">
            @include('layouts.backend.messages')
            <div class="row">
                <div class="col-md-3 logo"><img class="img-fluid" src="{{asset('backend/images/vespaio-admin-logo.png')}}"></div>
                <div class="col-md-9 login-form">
                    <h5>Vespaio Website Reset Password</h5>
                    
                        <form method="POST" action="{{ route('backend.users.new_password.save') }}" class="form-horizontal">
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="form-group form-inline">
                                <label for="email">Email address</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}"" required autocomplete="email" autofocus>
                                
                                <label for="password">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                
                                <label for="password-confirm">{{ __('Confirm Password') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" style="margin-bottom:0px;" required autocomplete="new-password">

                            </div>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <input type="submit" class="btn btn-md btn-outline-primary" value="{{ __('Save Password') }}">
                            <a class="btn btn-link" href="{{ route('login') }}">
                                {{ __('Back to Login?') }}
                            </a>
                            
                        </form>
                    
                    
                </div>
            </div>
        </div>    
    </section>


@endsection

