@extends('layouts.backend.master')
@section('pagetitle', 'Plans Detail')
@section('css')
    <style>

        tr#trFeature td input {text-align: left;}
        td#tdPlanintroS1 textarea {  text-align: left;vertical-align: top;}
        .floor .main .detail tr td:first-child {text-align: left;min-width: 160px;}
        tr#trFeature textarea {text-align: left;line-height: 22px;}
        td#tdFeature, #tdPlanintro { vertical-align: top;}

        .upperscroll,
        .tablewrap{ overflow-y:hidden;}

        .upperscroll{overflow-x: scroll;height: 20px; width: 100%;margin-bottom: 10px;}
        .tablewrap {overflow-x: hidden;}

        .upperscroll_content {width:3580px; height: 20px; }
        .tablewrap_content {width:3580px; overflow: auto;}

    </style>
@endsection
@section('content')


    <!-- Page Content Starts -->
    <div class="container firstcontainer floor" id="floor-plan">
        <div class="row">
            <div class="col-md-12 main">
                <form method="POST" action="{{route('backend.floor-plans.save')}}" class="form-horizontal">
                    @csrf
                    <div class="savebtn">
                        <div style="display:inline-block" class="col-md-11 text-center">
                            @include('layouts.backend.messages')
                        </div>
                        <span class="pull-right">
                            <input type="submit" class="btn btn-default vbtn" value="SAVE">
                        </span>
                    </div>
                    <div class="upperscroll">
                        <div class="upperscroll_content">
                        </div>
                    </div>
                    <div class="tablewrap">
                        <div class="table-container tablewrap_content">
                            <table class="table tabel-responsive table-striped detail">
                                <thead>
                                <tr>
                                    <td width="20%">PLANS</td>
                                    @foreach($names as $id=>$name)
                                        <input name="id[]" value="{{$id}}" type="hidden"/>
                                        <td width="20%">{{$names[$id]}}</td>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                <!--Rent Range Start-->
                                <!--tr id="trRentrange">
                                    <td id="tdRentrange">
                                        RENT RANGE
                                    </td>

                                    @foreach($rent_ranges as $id=>$rent_range)
                                    <td id="tdRentrangeS1">
                                        <span>$</span><input style="width:90px" type="text" value="{{$rent_ranges[$id]}}" id="rentrangeS1" name="rent_range[]">
                                        </td>
                                    @endforeach
                                    </tr-->
                                <!--Rent Range End-->

                                <!--Deposit Start-->
                                <tr id="trDeposit">
                                    <td id="tdDeposite">
                                        DEPOSIT
                                    </td>

                                    @foreach($deposits as $id=>$deposit)
                                        <td id="tdDepositeS1">
                                            <span>$</span><input style="width:30px" type="text" value="{{$deposits[$id]}}" id="DepositeS1" name="deposit[]">
                                        </td>
                                    @endforeach
                                </tr>
                                <!--Deposite End-->

                                <!--Term Start-->
                                <tr id="trTerm">
                                    <td id="tdTerm">
                                        TERM
                                    </td>

                                    @foreach($terms as $id=>$term)
                                        <td id="tdTermS1">
                                            <input style="width:50px" type="text" value="{{$terms[$id]}}" id="TermS1" name="term[]"><span>Mths</span>
                                        </td>
                                    @endforeach
                                </tr>
                                <!--Term End-->

                                <!--Appfee Start-->
                                <tr id="trAppfee">
                                    <td id="tdAppfee">
                                        APP FEE
                                    </td>

                                    @foreach($app_fees as $id=>$app_fee)
                                        <td id="tdAppfeeS1">
                                            <span>$</span><input style="width:30px" type="text" value="{{$app_fees[$id]}}" id="AppfeeS1" name="app_fee[]">
                                        </td>
                                    @endforeach
                                </tr>
                                <!--Appfee End-->

                                <!--Dogdeposit Start-->
                                <tr id="trDogdeposit">
                                    <td id="tdDogdeposit">
                                        DOG DEPOSIT
                                    </td>

                                    @foreach($dog_deposits as $id=>$dog_depost)
                                        <td id="tdDogdepositS1">
                                            <span>$</span><input style="width:40px" type="text" value="{{$dog_deposits[$id]}}" id="DogdepositS1" name="dog_deposit[]">
                                        </td>
                                    @endforeach
                                </tr>
                                <!--Dogdeposit End-->

                                <!--Catdeposit Start-->
                                <tr id="trCatdeposit">
                                    <td id="tdCatdeposit">
                                        CAT DEPOSIT
                                    </td>

                                    @foreach($cat_deposits as $id=>$cat_deposit)
                                        <td id="tdCatdepositS1">
                                            <span>$</span><input style="width:40px" type="text" value="{{$cat_deposits[$id]}}" id="CatdepositS1" name="cat_deposit[]">
                                        </td>
                                    @endforeach
                                </tr>
                                <!--Catdeposit End-->

                                <!--Petrent Start-->
                                <tr id="trPetrent">
                                    <td id="tdPetrent">
                                        PET RENT
                                    </td>

                                    @foreach($pet_rents as $id=>$pet_rent)
                                        <td id="tdPetrentS1">
                                            <span>$</span><input style="width:30px" type="text" value="{{$pet_rents[$id]}}" id="PetrentS1" name="pet_rent[]">
                                        </td>
                                    @endforeach
                                </tr>
                                <!--Petrent End-->

                                <!--Planintro Start-->
                                <tr id="trPlanintro">
                                    <td id="tdPlanintro">
                                        PLAN INTRO
                                    </td>

                                    @foreach($plan_intros as $id=>$plan_intro)
                                        <td id="tdPlanintroS1">
                                            <textarea id="PlanintroS1" name="plan_intro[]" rows="5">{{$plan_intros[$id]}}</textarea>
                                        </td>
                                    @endforeach
                                </tr>
                                <!--Planintro End-->

                                <!--Feature Start-->
                                <tr id="trFeature">
                                    <td id="tdFeature">
                                        FEATURE
                                    </td>

                                    @foreach($features as $id=>$feature)
                                        <td id="tdFeatureS1">
                                            <textarea id="feature_S1" name="feature[]" rows="10">{{$features[$id]}}</textarea>
                                        </td>
                                    @endforeach
                                </tr>
                                <!--Feature End-->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>

                <div class="col-md-8">
                    <span class="pull-left">
                         To edit information, click in a field that you want to change, update the content and select the save button.
                    </span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>

        let isSaved = false;

        let changes = 0;

        $(function(){
            $(".upperscroll").scroll(function(){
                $(".tablewrap").scrollLeft($(".upperscroll").scrollLeft());
            });
            $(".tablewrap").scroll(function(){
                $(".upperscroll").scrollLeft($(".tablewrap").scrollLeft());
            });

            $('form textarea').on('input', function(){
                changes++;
            });
            $('form input').on('input', function(){
                changes++;
            });

            $('.vbtn').click(()=>{
                isSaved = true;
            })
        });

        window.onbeforeunload = confirmExit;
        function confirmExit()
        {
            if(isSaved){
                return undefined;
            }
            if(changes>0){
                return "You have attempted to leave this page. If you have made any changes to the fields without clicking the Save button, your changes will be lost.  Are you sure you want to exit this page?";
            }
        }

    </script>
@endsection
