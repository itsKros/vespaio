
@extends('layouts.backend.master')
@section('pagetitle', 'Management & Leasing Team')
@section('css')
    <style>
        .modal-dialog { max-width: 665px;}

        footer.footer {position: relative;top: 290px;}
    </style>
@endsection
@section('content')

    <!-- Page Content Starts -->
    <div class="container firstcontainer team">
        <div class="row">
            <div class="col-md-10 text-center">
                @include('layouts.backend.messages')
            </div>
            <div class="col-md-2 adduser">
                <a href="" id="" data-toggle="modal" data-target="#exampleModal">
                    <div>
                        <img src="{{asset('backend/images/adduser.png')}}" alt="">
                        <span>Click to Add</span>
                    </div>
                </a>
            </div>
            <div class="col-md-12 main" id="table">
                <form method="POST" action="{{route('backend.users.save')}}" class="form-horizontal">
                    @csrf

                    <table class="table table-striped detail">
                        <thead>
                        <tr>
                            <td>NAME</td>
                            <td>USERNAME</td>
                            <td>USER TYPE</td>
                            <td>RECIEVE EMAILS</td>
                            <td>DELETE USER</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    <input type="hidden" name="id[]" value="{{$user->id}}">
                                    <input type="text" value="{{$user->name}}" id="name" name="name[]" required>
                                </td>
                                <td>
                                    <input type="email" value="{{$user->email}}" id="email" name="email[]" required>
                                </td>
                                <td>
                                    <div class="selectdiv">
                                        <label for="usertype">
                                            <select id="usertype" name="role_id[]" required>
                                                @foreach($roles as $role)
                                                    <option {{($role->id == $user->role->id)? 'selected' : ''}} value="{{$role->id}}">{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <label for="recievemail">
                                        <select class="short" id="recievemail" name="receive_email[]" required>
                                            <option {{ ($user->receive_emails == 1)? 'selected' : '' }} value="1">Yes</option>
                                            <option {{ ($user->receive_emails == 0)? 'selected' : '' }} value="0">No</option>
                                        </select>
                                    </label>
                                </td>

                                <td>
                                    @if($user->role_id !== 1)
                                        <button action="{{route('backend.users.delete', ['user'=>$user->id])}}" type="button" data-toggle="modal" data-target=".bs-confirm-modal" class="deluser">
                                            <img src="{{asset('backend/images/deleteuser.png')}}" alt="" class="deluser">
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="col-md-12">
                        <span class="pull-left">
                            Administrator can add, delete and update users in the table above. To edit user information, click in a field, update and select the save button.
                        </span>
                        <span class="pull-right">
                            <input type="submit" class="btn btn-default vbtn" value="SAVE">
                        </span>
                    </div>
                </form>
                @include('layouts.backend.confirm')
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST" action="{{route('backend.users.store')}}" class="form-horizontal" @submit="validateAddUserForm">
                    @csrf
                    <div class="modal-body">
                        <div style="text-align: center;">
                            <ul >
                                <li v-for="error in errors" class="error">@{{error}}</li>
                            </ul>
                        </div>

                        <table class="table table-striped detail">
                            <thead>
                            <tr>
                                <td colspan="2" style="text-align:center;">CREATE NEW USER</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><label for="name">NAME</label></td>
                                <td><input v-model="name" type="text" id="name" name="name" placeholder="Type First and Last Name" required minlength="3"></td>
                            </tr>
                            <tr>
                                <td><label for="email">EMAIL</label></td>
                                <td><input v-model="email" type="email" id="email" name="email" placeholder="Type Email Address" required></td>
                            </tr>
                            <tr>
                                <td><label for="cemail">CONFIRM EMAIL</label></td>
                                <td><input v-model="confirmEmail" type="text" id="cemail" name="email_confirmation" placeholder="Type Email Address" required></td>
                            </tr>
                            <tr>
                                <td><label for="usertype">USER TYPE</label></td>
                                <td>
                                    <label for="usertype">
                                        <select  style="width:175px" v-model="userType" class="short" id="usertype" name="role_id" required>
                                            <option selected value="-1">Select</option>
                                            @foreach($roles as $role)
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="recievemail">RECEIVE EMAIL</label></td>
                                <td>
                                    <label for="recievemail">
                                        <select style="width:175px" v-model="receiveEmail" class="short" id="recievemail" name="receive_email" required>
                                            <option selected value="-1">Select</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </label>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <span class="pull-left">To create a new user, add the required information above and select the save button.  An email will be sent to the new user that includes a link for creating thier password. </span>
                        <input type="submit" value="SAVE" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>


       const modal = new Vue({
            el: '#exampleModal',
            data: {
                errors: [],
                name: null,
                email: null,
                confirmEmail: null,
                userType: -1,
                receiveEmail:-1
            },
            methods:{
                validateAddUserForm: function (e) {
                    this.errors = [];
                    if(this.userType === -1 && this.receiveEmail === -1){
                        this.errors.push('All fields are required!');
                    }
                    if(this.email !== this.confirmEmail){
                        this.errors.push('Email and Confirm Email fields should match!');
                    }

                    if(this.errors.length === 0){
                        return true;
                    }
                    e.preventDefault();
                }
            }
        });

       var isSaved = false;

       let changes = 0;

       $(function(){
           $('form select').on('change', function(){
               changes++;
           });

           $('form input').on('input', function(){
               changes++;
           });

           $('.vbtn').click(()=>{
               isSaved = true;
           })
       });

       window.onbeforeunload = confirmExit;
       function confirmExit()
       {
           if(isSaved){
               return undefined;
           }
           if(changes>0){
               return "You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save button, your changes will be lost.  Are you sure you want to exit this page?";
           }
       }
    </script>
@endsection
