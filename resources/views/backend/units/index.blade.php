@extends('layouts.backend.master')

@section('pagetitle', 'Units')

@section('css')
    <link rel="stylesheet" href="{{asset('backend/css/unittable.css')}}">
    <style>
        body{overflow-x: hidden;}
        .firstcontainer {padding: 75px 0 55px;height:89vh;}
        td#tdDate { min-width: 123px;}
        .pagination {justify-content: space-between;padding: 0 15px;}
        .unit .sidebar .box{clear: both;}
        .unit .sidebar .pull-right{float:none;display: block;text-align: right;}

        @media only screen and (min-width:1400px) {
            .unit .sidebar .pull-right{margin-right:7px;}
        }
        @media only screen and (min-width:1900px) {
            .unit .sidebar .pull-right{margin-right:20px;}
        }
    </style>
@endsection

@section('content')

    <!-- Page Content Starts -->
    <div class="container firstcontainer unit">
        <div class="row justify-content-center">

            <div class="col-md-3 sidebar">
                <h5 class="vtitle">Filter Units</h5>
                <div class="box">
                    <form method="GET" action="{{route('backend.units.filter')}}" class="form-horizontal">
                        <table class="table sideform">
                            <tbody>
                            <tr>
                                <td><label for="searchunit">Search Unit #</label></td>
                                <td><input value="{{request('name')}}" id="filter-name" type="text" class="form-control" name="name"></td>
                            </tr>
                            <tr>
                                <td><label for="allavailable">All Available</label></td>
                                <td>
                                    <div class="round">
                                        <input type="radio" id="allacheck" name="unit_check" value="1" {{(request('unit_check') == 1)? 'checked' : ''}}/>
                                        <label for="allacheck"></label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="allunit">All Units</label></td>
                                <td >
                                    <div class="round">
                                        <input type="radio" id="allucheck" name="unit_check" value="0" {{(request('unit_check') == 0)? 'checked' : ''}}/>
                                        <label for="allucheck"></label>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td><label for="floor">Floor</label></td>
                                <td><input id="filter-floor-no" type="number" class="form-control" name="floor_no" value="{{request('floor_no')}}" min="3" max="7"></td>
                            </tr>
                            <tr>
                                <td><label for="view">View</label></td>
                                <td>
                                    <select id="filter-view-id"  name="view_id">
                                        <option value="0">Select View</option>
                                        @foreach($views as $view)
                                            <option {{($view->id == request('view_id'))? 'selected' : ''}} value="{{$view->id}}">{{$view->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="colorscheme">Color Scheme</label></td>
                                <td>
                                    <select id="filter-scheme-id" name="scheme_id" class="w-100">
                                        <option value="0">Select Scheme</option>
                                        @foreach($schemes as $scheme)
                                            <option {{($scheme->id == request('scheme_id'))? 'selected' : ''}} value="{{$scheme->id}}">{{$scheme->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <span class="pull-right">
                            <input type="submit" class="btn btn-default vbtn" value="FILTER">
                        </span>
                    </form>
                </div>
            </div>
            <div class="col-md-9 main">
                <form id="unit-list" method="POST" action="{{route('backend.units.save')}}" class="form-horizontal">
                    @csrf
                    <div class="saveWrap">
                        <span class="pull-left w-100">
                            @include('layouts.backend.messages')
                        </span>
                        <span class="pull-right">
                            <input type="submit" class="btn btn-default vbtn" value="SAVE" >
                        </span>
                    </div>
                    <table class="table table-responsive table-striped detail">
                        <thead>
                        <tr>
                            <td id="tdUnit">UNIT</td>
                            <td id="tdShow">SHOW</td>
                            <td id="tdList">LIST 1st</td>
                            <td id="tdDate">DATE AVAIL.</td>
                            <td id="tdRent">RENT</td>
                            <td id="tdPlan">PLAN</td>
                            <td id="tdScheme">SCHEME</td>
                            <td id="tdView">VIEW</td>
                            <td id="tdFloor">FLOOR</td>
                            <td id="tdF1">FEATURE 1</td>
                            <td id="tdF2">FEATURE 2</td>
                            <td id="tdF3">FEATURE 3</td>
                        </tr>
                        </thead>
                        <tbody>
                        <!--A100 Starts-->
                        @foreach($units as $index=>$unit)
                            <tr id="trA100">
                                <td id="tdA100unit"><input type="hidden" name="id[]" value="{{$unit->id}}"/><input type="text" name="name[]" value="{{$unit->name}}"/></td>

                                <td id="tdA100show">
                                    <div class="round">
                                        <input type="checkbox" name="show[]" id="show-{{$unit->id}}" value="{{$unit->id}}" {{($unit->show === 1)? 'checked' : ''}}/>
                                        <label for="show-{{$unit->id}}"></label>
                                    </div>
                                </td>

                                <td id="tdA100list">
                                    <div class="round">
                                        <input type="checkbox" name="list_1st[]" id="list-1st-{{$unit->id}}" value="{{$unit->id}}" {{($unit->list_1st === 1)? 'checked' : ''}}/>
                                        <label for="list-1st-{{$unit->id}}"></label>
                                    </div>
                                </td>

                                <td id="tdA100dateavail">
                                    @php
                                        $date_available = \Carbon\Carbon::parse($unit->date_available);
                                        if($unit->date_available === null){
                                            $date_available = 'Leased';
                                        }else{
                                             $diff_in_days = \Carbon\Carbon::today()->diffInDays($date_available, false);
                                            if($diff_in_days>0){
                                                $date_available = $date_available->format('m/d/Y');
                                            }else{
                                                $date_available = 'Now';
                                            }
                                        }
                                    @endphp
                                    <input class="unitsdatepicker" type="text" name="date_available[]" value="{{$date_available}}" id="a100dateavail" readonly/>
                                </td>

                                <td id="tdA100rent">
                                    <div class="tdRent">
                                        <span @if(empty($unit->rent)) style="display:none;" @endif>$</span>
                                        <input class="rent-input" type="text" name="rent[]" value="{{($unit->rent)? $unit->rent : ''}}" id="a100rent" name="a100rent"/>
                                    </div>
                                </td>

                                <td id="tdA100plan">
                                    <select id="a100plan" name="floor_plan_id[]">
                                        <option selected value="0"></option>
                                        @foreach($floor_plans as $plan)
                                            <option {{($plan->id === $unit->floor_plan_id)? 'selected' : ''}} value="{{$plan->id}}">{{$plan->name}}</option>
                                        @endforeach
                                    </select>
                                </td>

                                <td id="tdA100scheme">
                                    <select id="a100scheme" name="scheme_id[]">
                                        <option selected value="0"></option>
                                        @foreach($schemes as $scheme)
                                            <option {{($scheme->id === $unit->scheme_id)? 'selected' : ''}} value="{{$scheme->id}}">{{$scheme->name}}</option>
                                        @endforeach
                                    </select>
                                </td>

                                <td id="tdA100view">
                                    <select id="a100view" name="view_id[]">
                                        <option selected value="0"></option>
                                        @foreach($views as $view)
                                            <option {{($view->id === $unit->view_id)? 'selected' : ''}} value="{{$view->id}}">{{$view->name}}</option>
                                        @endforeach
                                    </select>
                                </td>

                                <td id="tdA100Floor">
                                    <input type="number" name="floor_no[]" value="{{$unit->floor_no}}" id="a100floor" min="3" max="7"/>
                                </td>

                                <td id="tdA100f1">
                                    <input type="text" name="feature_1[]" value="{{$unit->feature_1}}" id="a100f1"/>
                                </td>

                                <td id="tdA100f2">
                                    <input type="text" name="feature_2[]" value="{{$unit->feature_2}}" id="a100f2" />
                                </td>

                                <td id="tdA100f3">
                                    <input type="text" name="feature_3[]" value="{{$unit->feature_3}}" id="a100f3" />
                                </td>
                            </tr>
                            <!--A100 Ends-->
                        @endforeach
                        </tbody>
                    </table>
                    @if(count($units)>0)
                    <div style="padding-bottom: 20px;">
                    Showing {{($units->currentPage()*$units->perPage())-($units->perPage()-1)}} to {{(($units->currentPage()-1)*$units->perPage()+count($units))}} of total {{$units->total()}}
                    </div>
                    @endif
                </form>
            </div>

            <div class="col-md-9 offset-md-3 pagination">
                <div class="pull-left">
                    To edit information, click in a field that you want to change, update the content, and select the save button.
                </div>
                @if($units->total()>$units->perPage())
                <div class="arrow pull-right">
                  {{-- {{$units->appends(request()->except('token'))->links()}}--}}
                    <a href="{{$units->previousPageUrl()}}" @if($units->currentPage()===1) class="disabled" @endif>
                        <img src="{{asset('backend/images/caret-left.png')}}" alt="">
                    </a>
                    <a href="{{$units->nextPageUrl()}}" @if($units->currentPage()===$units->lastPage()) class="disabled" @endif>
                        <img src="{{asset('backend/images/caret-right.png')}}" alt="">
                    </a>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        var isSaved = false;

        var changes = 0;

        var date = new Date();
        date.setDate(date.getDate() + 90);
        $(".detail tbody tr td:nth-child(4) input").datetimepicker({
            format: 'mm/dd/yyyy',
            minView:'month',
            maxView: 'year',
            autoclose: true,
            startView:2,
            todayHighlight:true,
            startDate:new Date(),
            endDate : date
        });


        $(document).ready(function(){
            //Empty selected option remove select tag dropdown arrow
            $('.unit .main form .detail tbody tr td select').change(function () {
                if($(this).val() === "0"){ $(this).css('background','none');}
                else {$(this).css('background','url(/backend/images/caret-down.png) no-repeat right center');}
            }).trigger('change');

            //Empty available date field remove calender icon
            $('.detail tbody tr td:nth-child(4) input').change(function () {
                $(this).each(function(){
                    if ($(this).val() === "" ){
                        $(this).css('background','none');
                    } else {
                        $(this).css('background','url(/backend/images/calendar.png) no-repeat 97% 50%');
                    }
                });
            }).trigger('change');

            $('.rent-input').on('blur', function(){
                $rent = $(this).val();
                if($rent!==''){
                    $(this).siblings('span').show();
                }else{
                    $(this).siblings('span').hide();
                }
            });

            $('#unit-list select').on('change', function(){
                changes++;
            });
            $('#unit-list input').on('input', function(){
                changes++;
            });
            $('.unitsdatepicker').on('change', function(){
                changes++;
            });
            $('.vbtn').click(()=>{
                isSaved = true;
            });
        });

        window.onbeforeunload = confirmExit;
        function confirmExit()
        {
            if(isSaved){
                return undefined;
            }
            if(changes>0){
                return "You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save button, your changes will be lost.  Are you sure you want to exit this page?";
            }
        }
    </script>
@endsection
