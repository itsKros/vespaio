@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            User Registration
        @endcomponent
    @endslot
    {{-- Body --}}
    <h2>Welcome to Vespio CMS!</h2>

    You have been registered on Vespaio as {{$user->role->name}}. To create your password please follow this {{route('backend.users.new_password', ['token'=>$user->new_password_token])}}

    Regards
    Vespaio Team
    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset
    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}.
        @endcomponent
    @endslot
@endcomponent
