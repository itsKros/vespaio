@extends('layouts.backend.master')

@section('pagetitle', 'Dashboard')
@section('headjs')

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

        var units_by_plan_type = JSON.parse('{!!json_encode($units_by_plan_type)!!}');
        var units_by_plan_name = JSON.parse('{!!json_encode($units_by_plan_name)!!}');
        var units_by_rent_range = JSON.parse('{!!json_encode($units_by_rent_range)!!}');
        var units_by_floor_no = JSON.parse('{!!json_encode($units_by_floor_no)!!}');
        var units_by_view = JSON.parse('{!!json_encode($units_by_view)!!}');
        var units_by_scheme = JSON.parse('{!!json_encode($units_by_scheme)!!}');

       console.log(units_by_view);

        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(planType);
        function planType() {
            var data = google.visualization.arrayToDataTable(units_by_plan_type);
            var options = {
                pieHole: 0.6,
                pieSliceText: 'value',
                legend: {
                    position: "bottom",
                }
            };
            var chart = new google.visualization.PieChart(document.getElementById('plantype'));
            chart.draw(data, options);
        }
        
        google.charts.setOnLoadCallback(rentRange);
        function rentRange() {
            var data = google.visualization.arrayToDataTable(units_by_rent_range);
            var options = {
                pieHole: 0.6,
                pieSliceText: 'value',
                legend: {
                    position: "bottom",
                }
            };
            var chart = new google.visualization.PieChart(document.getElementById('rentrange'));
            chart.draw(data, options);
        }


        google.charts.setOnLoadCallback(plan);
        function plan() {
            var data = google.visualization.arrayToDataTable(units_by_plan_name);
            var options = {
                pieHole: 0.6,
                pieSliceText: 'value',
                legend: {
                    position: "bottom",
                }
            };
            var chart = new google.visualization.PieChart(document.getElementById('plan'));
            chart.draw(data, options);
        }

        google.charts.setOnLoadCallback(floor);
        function floor() {
            var data = google.visualization.arrayToDataTable(units_by_floor_no);
            var options = {
                pieHole: 0.6,
                pieSliceText: 'value',
                legend: {
                    position: "bottom",
                }
            };
            var chart = new google.visualization.PieChart(document.getElementById('floor'));
            chart.draw(data, options);
        }
        
        google.charts.setOnLoadCallback(view);
        function view() {
            var data = google.visualization.arrayToDataTable(units_by_view);
            
            var options = {
                pieHole: 0.6,
                pieSliceText: 'value',
                legend: {
                    position: "bottom",
                }
            };
            var chart = new google.visualization.PieChart(document.getElementById('view'));
            chart.draw(data, options);
        }


        google.charts.setOnLoadCallback(color);
        function color() {
            var data = google.visualization.arrayToDataTable(units_by_scheme);
            var options = {
                pieHole: 0.6,
                pieSliceText: 'value',
                legend: {
                    position: "bottom",
                }
            };
            var chart = new google.visualization.PieChart(document.getElementById('color'));
            chart.draw(data, options);
        }


    </script>

@endsection

@section('css')
<style>
    .box {border: 1px solid #bdbdbd;position: relative;}
    .box h3 {position: absolute;top: 18px;left: 50%;z-index: 1;transform: translateX(-50%);font-size: 17px;color: #bdbdbd;}
</style>
@endsection

@section('content')

<!-- Page Content Starts -->
<div class="container firstcontainer dashboard">
    <div class="row justify-content-center">
        <div class="col-md-4 text-center">
            <div class="box">
                <h3>Plan Type</h3>
                <div id="plantype" style="width: 100%; height: 250px;"></div>
            </div>
        </div>
        <div class="col-md-4 text-center">
            <div class="box">
                <h3>Rent Range</h3>
                <div id="rentrange" style="width: 100%; height: 250px;"></div>
            </div>
        </div>
        <div class="col-md-4 text-center">
            <div class="box">
                <h3>Plan</h3>
                <div id="plan" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

        <div style="width:100%; height:30px;"></div>

        <div class="col-md-4 text-center">
            <div class="box">
                <h3>Floor</h3>
                <div id="floor" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

        <div class="col-md-4 text-center">
            <div class="box">
                <h3>View</h3>
                <div id="view" style="width: 100%; height: 250px;"></div>
            </div>
        </div>

        <div class="col-md-4 text-center">
            <div class="box">
                <h3>Color Scheme</h3>
                <div id="color" style="width: 100%; height: 250px;"></div>
            </div>
        </div>
        <div style="width:100%; height:60px;"></div>    

    </div>
</div>
@endsection

@section('js')

@endsection
