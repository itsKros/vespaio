@extends('layouts.backend.master')
@section('pagetitle', 'Leasing & Policies')
@section('css')
    <style>
        .modal-dialog {
            max-width: 665px;
        }


    </style>
@endsection
@section('content')


    <!-- Page Content Starts -->
    <form method="POST" action="{{route('backend.specials.update', ['special'=>1])}}" class="form-horizontal">
        {{ method_field('PUT')}}
        @csrf
    <div class="container lease">
        <div class="row">
            <div class="col-md-2 offset-md-10 savebutton">
                <input class="btn vbtn" type="submit" value="SAVE">
            </div>

                <div class="col-md-6 text">
                    <div class="box">
                        <h5 class="vtitle">SPECIALS<span class="pull-right">Show Specials <input name="show_specials" type="checkbox" {{($specials->show_specials===1)? 'checked' : ''}}></span></h5>
                        <div class="tbox">
                            <textarea name="specials" id="specials">{{$specials->specials}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text">
                    <div class="box">
                        <h5 class="vtitle">LEASING TERMS</h5>
                        <div class="tbox">
                            <textarea name="leasing_terms" id="leasing">{{$specials->leasing_terms}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text">
                    <div class="box">
                        <h5 class="vtitle">PET POLICY</h5>
                        <div class="tbox">
                            <textarea name="pet_policy" id="pet">{{$specials->pet_policy}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text">
                    <div class="box">
                        <h5 class="vtitle">PARKING POLICY</h5>
                        <div class="tbox">
                            <textarea name="parking_policy" id="parking">{{$specials->parking_policy}}</textarea>
                        </div>
                    </div>
                </div>

            <div class="col-md-12 info">
                <p>To create or edit the leasing special which appears on the website just type the special below, formate the text and select the save button.  Check the Show Specials box.  Proof special on the website.</p>
            </div>
        </div>
    </div>
    </form>

@endsection
@section('js')
    <script>

        var isSaved = false;

        let changes = 0;

        $(document).ready(function(){
            CKEDITOR.replace( 'specials');
            CKEDITOR.replace( 'leasing' );
            CKEDITOR.replace( 'pet' );
            CKEDITOR.replace( 'parking' );

            CKEDITOR.instances.specials.on('change', function(){changes++;});
            CKEDITOR.instances.leasing.on('change', function(){changes++;});
            CKEDITOR.instances.pet.on('change', function(){changes++;});
            CKEDITOR.instances.parking.on('change', function(){changes++;});

            $('.vbtn').click(()=>{
                isSaved = true;
            })
        });

        window.onbeforeunload = confirmExit;
        function confirmExit()
        {
            if(isSaved){
                return undefined;
            }
            if(changes>0){
                return "You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save button, your changes will be lost.  Are you sure you want to exit this page?";
            }
        }

    </script>
@endsection
