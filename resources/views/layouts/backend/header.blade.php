<header id="mainheader">
    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container-fluid">
            {{-- <a class="navbar-brand" href="#">Navbar</a> --}}
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <h1 class="vespaio-brand">VESPAIO | <span>@yield('pagetitle')</span></h1>
                <ul class="navbar-nav ml-auto">
                    
                    <li class="nav-item {{ $routename == 'backend.units.filter' ? 'active' : '' }}">
                        <a class="nav-link" href="{{route('backend.units.filter')}}">UNITS</a>
                    </li>
                    <li class="nav-item {{ $routename == 'backend.floor-plans.index' ? 'active' : '' }}">
                        <a class="nav-link" href="{{route('backend.floor-plans.index')}}">PLAN DETAILS</a>
                    </li>
                    <li class="nav-item {{ $routename == 'backend.specials.index' ? 'active' : '' }}">
                        <a class="nav-link" href="{{route('backend.specials.index')}}">LEASING & POLICIES</a>
                    </li>
                    
                    <li class="nav-item {{ $routename == 'backend.home' ? 'active' : '' }}">
                        <a class="nav-link" href="{{route('backend.home')}}">DASHBOARD</a>
                    </li>

                    <li class="nav-item {{ $routename == 'backend.users.index' ? 'active' : '' }}">
                        <a class="nav-link" href="{{route('backend.users.index')}}">TEAM</a>
                    </li>
                    
                    
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}" onclick="
                        event.preventDefault();
                        document.getElementById('logout-form').submit();">
                                {{ __('LOGOUT') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
