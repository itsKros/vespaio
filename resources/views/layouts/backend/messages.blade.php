@if(count($errors) > 0)
    @foreach($errors->all() as $error)
    <div class="alert alert-danger">
        {{$error}}
    </div>
    @endforeach
@endif

@if(session('status'))
    <div class="alert alert-success">
        {{session('status')}}
    </div>
@endif

@if(session('success'))
    <div class="alert alert-success">
        {{session('status')}}
    </div>
@endif

@if(session()->has('message.level'))
    @if(session('message.level') === 'success')
        <div class="alert alert-success">
            {!! session('message.content') !!}
        </div>
    @endif

    @if(session('message.level') === 'danger')
        <div class="alert alert-danger">
            {!! session('message.content') !!}
        </div>
    @endif
@endif


@if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
@endif
