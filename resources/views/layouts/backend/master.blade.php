@php
 $routename = Route::currentRouteName();
@endphp
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>Vespaio</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
        <link rel="stylesheet" href="{{asset('backend/datetimepicker/bootstrap-datetimepicker.min.css')}}">
        <link rel="stylesheet" href="{{asset('backend/css/style.css')}}">
        @yield('css')
        @yield('headjs')
    
    </head>
    <body>
        @include('layouts.backend.header')

        @yield('content')

        @include('layouts.backend.footer')

        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" ></script>
        <script src="{{asset('backend/datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
        <script src="//cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
        <script src="{{asset('backend/js/custom.js')}}"></script>
        @yield('js')
    </body>
</html>
