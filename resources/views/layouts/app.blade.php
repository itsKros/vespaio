
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Vespaio</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
        <link rel="stylesheet" href="{{asset('frontend/css/style.css')}}">
        @yield('css')
    
    </head>
    <body>
        @if (Route::has('login'))
<div class="top-right links">
    @auth
        <a href="{{ route('frontend.home') }}">Home</a>
    <a class="" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
        
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
    @else
        <a href="{{ route('login') }}">Login</a>

        @if (Route::has('register'))
            <a href="{{ route('register') }}">Register</a>
        @endif
    @endauth
</div>
@endif
<footer class="footer"> © <?php echo date('Y'); ?> {{ config('app.name') }} </footer>

<script src="{{asset('frontend/js/custom.js')}}"></script>
        @yield('js')
    </body>
</html>