<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Vespaio</title>

        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/animate.css@3.5.1">
        <link rel="stylesheet" type="text/css" href="{{asset('frontend/owl/owl.carousel.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('frontend/owl/owl.theme.default.min.css')}}">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pannellum@2.5.2/build/pannellum.css">
        <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/master.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/mstyle.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/svg.css')}}">
        @yield('css')

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfcyG22cA1miqTixO0wK9k_78xmh_4wKE&libraries=places"></script>
    
    </head>
    <body>
        @include('layouts.frontend.header')

        @yield('content')

        @include('layouts.frontend.footer')
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
        <script src="{{asset('frontend/owl/owl.carousel.min.js')}}"></script>
        <script src="{{asset('frontend/nicescroll/jquery.nicescroll.min.js')}}"></script>
        <script src="https://cdn.jsdelivr.net/npm/pannellum@2.5.2/build/pannellum.js"></script>
        <script src="{{asset('frontend/js/view.js')}}"></script>
        <script src="{{asset('frontend/js/script.js')}}"></script>
        @yield('js')
    </body>
</html>
