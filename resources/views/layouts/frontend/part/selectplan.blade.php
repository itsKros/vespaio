<transition  name="custom-classes-transition"
             enter-active-class="animated fadeIn"
             leave-active-class="animated fadeOut">
    <div class="box sitecontent" v-if="activeUnit !== null" v-show="isFloorPlan">
        <div class="header">
            <div class="info">
                <span>1</span>
                <p>Select your favourite plan from above.</p>
            </div>
            <div class="info">
                <span>2</span>
                <p>Apply filters in the Home Search.</p>
            </div>
            <div class="showing" v-html="getFilterString()">

            </div>
            <div class="fdClose" @click="isFloorPlan = !isFloorPlan">
                <span>X</span>
            </div>
        </div>
        <div class="floordetail">
            <div class="floor">

            </div>
            <div class="detail">
                <div class="quick">
                    <h4>FLOOR</h4>
                    <div v-for="floor in floors" v-if="floor.id>0" class="qbox" :class="{selected : activeUnit.floor_no == floor.id}" @click="floorClicked(floor)">
                        <span>@{{floor.id}}</span>
                        <span>@{{getAvailableUnitsInFloor(floor.id)}} units</span>
                    </div>
                </div>
                <div class="aptqk" v-if="detailedUnit!=null">
                    <h5>APT <span>@{{ detailedUnit.name }}</span></h5>
                    <div class="con" >
                        <img src="{{asset('frontend/img/thumb-apt.jpg')}}" alt="">
                        <div>
                            <span>@{{detailedUnit.floor_plan.name}} | @{{detailedUnit.floor_plan.bdbth}}</span>
                            <span>@{{ detailedUnit.sqft }}</span>
                            <span>@{{ unitAvailability }}</span>
                            <span>$@{{detailedUnit.rent}}</span>
                            <span>@{{ detailedUnit.scheme.name }} Scheme</span>
                        </div>
                        <p @click="isFloorPlan = !isFloorPlan">CLICK TO VIEW</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</transition>
