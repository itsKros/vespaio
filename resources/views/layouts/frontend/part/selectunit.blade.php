<div class="selectunit">
    <div class="title">
        <span>SELECT PLAN</span>
    </div>

    <div class="units">
        <div class="owl-carousel unitscroller">
            <div class="unit" v-for="floorPlan in floorPlans">
                <input type="checkbox" :id="'floor_plan_'+floorPlan.id" :value="floorPlan.id" v-model="floorPlan.checked" @click="filterClicked(floorPlan, floorPlans)"/>
                <label :for="'floor_plan_'+floorPlan.id">
                    <span class="name">@{{floorPlan.name}}</span>
                    <span class="desc">@{{floorPlan.bdbth}}</span>
                </label>
            </div>
        </div>
    </div>

    

    <div class="infosection">
        <div class="icon">
            <div class="threebtn img map" @click="mapIcon">
                <img v-if="!isFloorPlan" src="{{asset('frontend/img/Map_icon_grey.png')}}"/>
                <img v-if="isFloorPlan" src="{{asset('frontend/img/Map_icon_blue.png')}}"/>
            </div>
            <div class="threebtn big"><a href="#">Apply<br>Online</a></div>
            <div class="threebtn img info" @click="infoIcon">
                <img v-if="!iIcon" src="{{asset('frontend/img/i_grey.png')}}"/>
                <img v-if="iIcon" src="{{asset('frontend/img/i_blue.png')}}"/>
            </div>
        </div>
        <div class="infoitems" v-if="isInfoItems">
            <div id="specials" @click="specialBtn" :class="[isSpecial ? 'selected' : '']">SPECIALS</div>
            <div id="pets" @click="petBtn" :class="[isPet ? 'selected' : '']">PETS</div>
            <div id="leasing" @click="leasingBtn" :class="[isLeasing ? 'selected' : '']">LEASING</div>
            <div id="parking" @click="parkingBtn" :class="[isParking ? 'selected' : '']">PARKING</div>
        </div>
        
        <div class="infodata" v-if="isInfoData">
            <span class="closeBtn" @click="infoData">&times;</span>
            <div v-if="isSpecial">{!! $specials->specials !!}</div>
            <div v-if="isPet">{!! $specials->pet_policy !!}</div>
            <div v-if="isLeasing">{!! $specials->leasing_terms !!}</div>
            <div v-if="isParking">{!! $specials->parking_policy !!}</div>
        </div>
    </div>
</div>
