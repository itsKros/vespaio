
<div class="searchfilter">
    <div class="title">
        <span>FILTER HOMES</span>
    </div>

    <div class="filterbtn">
        <div class="slab openclose" @click="filterBtn(filterBtnVal)">
            <div class="namefilter">
                <span class="opc">@{{filterBtnVal}} ALL</span>
            </div>
        </div>

        <div class="slab floor" >
            <div class="fwrap" v-on:click="isFloorHidden = !isFloorHidden" >
                <div class="namefilter">
                    <span>FLOOR</span>
                    <span>@{{getCheckedFilters(this.floors)}}</span>
                </div>
                <i class="fa fa-plus"></i>
            </div>
            <transition :duration="{enter:200, leave:200}"  name="custom-classes-transition"
                        enter-active-class="animated bounceInUp"
                        leave-active-class="animated bounceOutDown">
                <div class="filteritems flrs" v-if="!isFloorHidden">
                    <div class="filter-content" v-for="floor in floors">
                        <p>@{{floor.name}}</p>
                        <div class="round">
                            <input type="checkbox" :id="'floor_'+floor.id" :value="floor.id" v-model="floor.checked" @click="filterClicked(floor, floors)"/>
                            <label :for="'floor_'+floor.id"></label>
                        </div>
                    </div>
                </div>
            </transition>
        </div>


        <div class="slab view">
            <div class="fwrap" v-on:click="isViewHidden = !isViewHidden" >
                <div class="namefilter">
                    <span>VIEW</span>
                    <span>@{{getCheckedFilters(this.views)}}</span>
                </div>
                <i class="fa fa-plus"></i>
            </div>

            <transition :duration="{enter:200, leave:200}"  name="custom-classes-transition"
                        enter-active-class="animated bounceInUp"
                        leave-active-class="animated bounceOutDown">
                <div class="filteritems views" v-if="!isViewHidden">
                    <div class="filter-content" v-for="view in views">
                        <p>@{{view.name}}</p>
                        <div class="round">
                            <input type="checkbox" :id="'view_'+view.id" :value="view.id" v-model="view.checked" @click="filterClicked(view, views)"/>
                            <label :for="'view_'+view.id"></label>
                        </div>
                    </div>
                </div>
            </transition>
        </div>
        <div class="slab scheme" >
            <div class="fwrap" v-on:click="isSchemeHidden = !isSchemeHidden" >
                <div class="namefilter">
                    <span>SCHEME</span>
                    <span>@{{getCheckedFilters(this.schemes)}}</span>
                </div>
                <i class="fa fa-plus"></i>
            </div>

            <transition :duration="{enter:200, leave:200}"  name="custom-classes-transition"
                        enter-active-class="animated bounceInUp"
                        leave-active-class="animated bounceOutDown">
                <div class="filteritems schm" v-if="!isSchemeHidden">
                    <div class="filter-content" v-for="scheme in schemes">
                        <p>@{{scheme.name}}</p>
                        <div class="round">
                            <input type="checkbox" :id="'scheme_'+scheme.id" :value="scheme.id" v-model="scheme.checked" @click="filterClicked(scheme, schemes)"/>
                            <label :for="'scheme_'+scheme.id"></label>
                        </div>
                    </div>
                </div>
            </transition>
        </div>
        <div class="slab avail">
            <div class="fwrap" v-on:click="isAvailableHidden = !isAvailableHidden" >
                <div class="namefilter">
                    <span>AVAILABLE</span>
                    <span>@{{getCheckedFilters(this.periods)}}</span>
                </div>
                <i class="fa fa-plus"></i>
            </div>
            <transition :duration="{enter:200, leave:200}"  name="custom-classes-transition"
                        enter-active-class="animated bounceInUp"
                        leave-active-class="animated bounceOutDown">
                <div class="filteritems avl" v-if="!isAvailableHidden">
                    <div class="filter-content" v-for="period in periods">
                        <p>@{{period.name}}</p>
                        <div class="round">
                            <input type="checkbox" :id="'period_'+period.id"  :value="period.id" v-model="period.checked" @click="filterClicked(period, periods)"/>
                            <label :for="'period_'+period.id"></label>
                        </div>
                    </div>
                </div>
            </transition>
        </div>
        <div class="slab rent">
            <div class="fwrap" v-on:click="isRentHidden = !isRentHidden" >
                <div class="namefilter">
                    <span>RENT</span>
                    <span>@{{getCheckedFilters(this.rentRanges)}}</span>
                </div>
                <i class="fa fa-plus"></i>
            </div>

            <transition :duration="{enter:200, leave:200}"  name="custom-classes-transition"
                        enter-active-class="animated bounceInUp"
                        leave-active-class="animated bounceOutDown">
                <div class="filteritems rnt" v-if="!isRentHidden">
                    <div class="filter-content" v-for="rentRange in rentRanges">
                        <p>@{{rentRange.name}}</p>
                        <div class="round">
                            <input type="checkbox" :id="'rent_range_'+rentRange.id" :value="rentRange.id" v-model="rentRange.checked" @click="filterClicked(rentRange, rentRanges)"/>
                            <label :for="'rent_range_'+rentRange.id"></label>
                        </div>
                    </div>
                </div>
            </transition>
        </div>
    </div>
    
</div>
