<div class="mobileheader ">
    <!--Plan Type Start-->
    <div class="borderbox plans" v-on:click="isListWrap = !isListWrap">
        <p class="title">PLAN TYPE</p><span class="ver"></span>
        <p class="name">2 Bedroom | <span>B2+B4</span></p><div class="arrow" :class="[isListWrap ? 'down' : 'up']"></div>
    </div>
    <div class="list-wrap" v-if="!isListWrap"> 
        <div class="list">
        <div class="unit" v-for="floorPlan in floorPlans">
                <input type="checkbox" :id="'floor_plan_'+floorPlan.id" :value="floorPlan.id" v-model="floorPlan.checked" @click="filterClicked(floorPlan, floorPlans)"/>
                <label :for="'floor_plan_'+floorPlan.id">
                    <span class="name">@{{floorPlan.name}} | @{{floorPlan.alias}}</span>
                    
                </label>
            </div>

        </div>
        <div class="closeBtn" v-on:click="isListWrap = !isListWrap">
            <p>Close List </p><p>X</p>
        </div>
    </div>
    <!--Plan Type End-->

    <!--Unit Scroll Start-->
    <div class="filterunit">
        <!--Filter Start-->
            <div class="icon filterbtn" :class="[!isFilterWrap ? 'active' : '']" @click="initiateSelect()">
                <img src="{{asset('frontend/img/mobile-filter.png')}}" alt="">
            </div>
            <div class="filter-wrap" v-if="!isFilterWrap">

            <!-- Filter Floor start -->
                <div class="filter floor">
                    <div class="fwrap" v-on:click="isFloorHidden = !isFloorHidden" >
                        <div class="namefilter">
                            <span>FLOOR | All</span>
                        </div>
                        <div class="arrow" :class="[!isFloorHidden ? 'up' : 'down']"></div>
                    </div> 
                    <transition :duration="{enter:200, leave:200}"  name="custom-classes-transition"
                                enter-active-class="animated bounceInUp"
                                leave-active-class="animated bounceOutDown">
                        <div class="filteritems flrs" v-if="!isFloorHidden">
                            <div class="filter-content" v-for="floor in floors">
                                <p>@{{floor.name}}</p>
                                <div class="round">
                                    <input type="checkbox" :id="'floor_'+floor.id" :value="floor.id" v-model="floor.checked" @click="filterClicked(floor, floors)"/>
                                    <label :for="'floor_'+floor.id"></label>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
            <!-- Filter Floor End -->

            <!-- Filter View start -->
                <div class="filter views">
                    <div class="fwrap" v-on:click="isViewHidden = !isViewHidden" >
                        <div class="namefilter">
                            <span>View | All</span>
                        </div>
                        <div class="arrow" :class="[!isViewHidden ? 'up' : 'down']"></div>
                    </div> 
                    <transition :duration="{enter:200, leave:200}"  name="custom-classes-transition"
                                enter-active-class="animated bounceInUp"
                                leave-active-class="animated bounceOutDown">
                        <div class="filteritems views" v-if="!isViewHidden">
                            <div class="filter-content" v-for="view in views">
                                <p>@{{view.name}}</p>
                                <div class="round">
                                    <input type="checkbox" :id="'view_'+view.id" :value="view.id" v-model="view.checked" @click="filterClicked(view, views)"/>
                                    <label :for="'view_'+view.id"></label>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
            <!-- Filter View End -->

            <!-- Filter Scheme start -->
                <div class="filter scheme">
                    <div class="fwrap" v-on:click="isSchemeHidden = !isSchemeHidden" >
                        <div class="namefilter">
                            <span>Scheme | All</span>
                        </div>
                        <div class="arrow" :class="[!isSchemeHidden ? 'up' : 'down']"></div>
                    </div> 
                    <transition :duration="{enter:200, leave:200}"  name="custom-classes-transition"
                                enter-active-class="animated bounceInUp"
                                leave-active-class="animated bounceOutDown">
                        <div class="filteritems schm" v-if="!isSchemeHidden">
                            <div class="filter-content" v-for="scheme in schemes">
                                <p>@{{scheme.name}}</p>
                                <div class="round">
                                    <input type="checkbox" :id="'scheme_'+scheme.id" :value="scheme.id" v-model="scheme.checked" @click="filterClicked(scheme, schemes)"/>
                                    <label :for="'scheme_'+scheme.id"></label>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
            <!-- Filter Scheme End -->

            <!-- Filter Available start -->
                <div class="filter avail">
                    <div class="fwrap" v-on:click="isAvailableHidden = !isAvailableHidden" >
                        <div class="namefilter">
                            <span>Available | All</span>
                        </div>
                        <div class="arrow" :class="[!isAvailableHidden ? 'up' : 'down']"></div>
                    </div> 
                    <transition :duration="{enter:200, leave:200}"  name="custom-classes-transition"
                                enter-active-class="animated bounceInUp"
                                leave-active-class="animated bounceOutDown">
                        <div class="filteritems avl" v-if="!isAvailableHidden">
                            <div class="filter-content" v-for="period in periods">
                                <p>@{{period.name}}</p>
                                <div class="round">
                                    <input type="checkbox" :id="'period_'+period.id"  :value="period.id" v-model="period.checked" @click="filterClicked(period, periods)"/>
                                    <label :for="'period_'+period.id"></label>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
            <!-- Filter Available End -->

            <!-- Filter Available start -->
                <div class="filter rent">
                    <div class="fwrap" v-on:click="isRentHidden = !isRentHidden" >
                        <div class="namefilter">
                            <span>Rent | All</span>
                        </div>
                        <div class="arrow" :class="[!isRentHidden ? 'up' : 'down']"></div>
                    </div> 
                    <transition :duration="{enter:200, leave:200}"  name="custom-classes-transition"
                                enter-active-class="animated bounceInUp"
                                leave-active-class="animated bounceOutDown">
                        <div class="filteritems rnt" v-if="!isRentHidden">
                            <div class="filter-content" v-for="rentRange in rentRanges">
                                <p>@{{rentRange.name}}</p>
                                <div class="round">
                                    <input type="checkbox" :id="'rent_range_'+rentRange.id" :value="rentRange.id" v-model="rentRange.checked" @click="filterClicked(rentRange, rentRanges)"/>
                                    <label :for="'rent_range_'+rentRange.id"></label>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
            <!-- Filter Available End -->
                

                <div class="closeBtn" v-on:click="isFilterWrap = !isFilterWrap">
                    <p>Close List </p><p>X</p>
                </div>
            </div>
        <!--Filter Ends-->

        <!--Units Start-->
            <div class="borderbox units" v-if="activeUnit != null">
                <div class="owl-carousel unitslide">
                    <div  v-for="unit in filteredUnits" class="unit" :class="{selected : activeUnit.id === unit.id}" @click="unitClicked(unit);">
                        @{{ unit.name }}
                    </div>
                </div>
            </div>

        <!--Units Ends-->

        <!--Match List Start-->
            <div class="icon filterbtn" :class="[!isMatchListWrap ? 'active' : '']" v-on:click="isMatchListWrap = !isMatchListWrap">
                <img src="{{asset('frontend/img/list-view-icon.png')}}" alt="">
            </div>
            <div class="matchlist-wrap" v-if="!isMatchListWrap">
                <div class="matchlist-title">
                    <div>Unit</div>
                    <div>Plan</div>
                    <div>Available</div>
                    <div>Rent</div>
                    <div>Scheme</div>
                </div>
                <div class="sorticon-warp">
                    <div class="sortico" @click="sortList('name')"><img :style="getSortRotation('name')" :src="getSortIcon('name')" alt=""></div>
                    <div class="sortico" @click="sortList('floor_plan')"><img :style="getSortRotation('floor_plan')" :src="getSortIcon('floor_plan')" alt=""></div>
                    <div class="sortico" @click="sortList('date_available')"><img :style="getSortRotation('date_available')" :src="getSortIcon('date_available')" alt=""></div>
                    <div class="sortico" @click="sortList('rent')"><img :style="getSortRotation('rent')" :src="getSortIcon('rent')" alt=""></div>
                    <div class="sortico" @click="sortList('scheme')"><img :style="getSortRotation('scheme')" :src="getSortIcon('scheme')" alt=""></div>
                </div>

                <table class="table table-responsive list" >
                    <tbody>
                    <tr  v-for="unit in filteredUnits" class="trdata" :class="{active : activeUnit.id === unit.id}" @click="activeUnit = unit">
                        <td>@{{  unit.name }}</td>
                        <td>@{{  (unit.floor_plan)? unit.floor_plan.name + '|' + unit.floor_plan.bdbth : 'N/A' }} </td>
                        <td>@{{ (unit.date_available)? getAvailabilityStr(unit.date_available) : 'N/A'}}</td>
                        <td>@{{ (unit.rent)? '$' + unit.rent : 'N/A' }}</td>
                        <td>@{{ (unit.floor_plan)? unit.scheme.name : 'N/A' }}</td>
                    </tr>
                    <tr v-if="filteredUnits.length === 0">
                        <td colspan="5" class="no-results">No homes match your search, adjust your filters!</td>
                    </tr>
                    </tbody>
                </table>
                <div class="closeBtn" v-on:click="isMatchListWrap = !isMatchListWrap">
                    <p>Close List </p><p>X</p>
                </div>
            </div>
        <!--Match List Ends-->
    </div>
    <!--Unit Scroll End-->
</div>

    <!-- Info Section Start-->
    <div class="infosection" v-if="activeUnit != null">
        <img src="{{asset('frontend/img/info.png')}}" alt="" v-on:click="isInfoWrap = !isInfoWrap">
        <p><span>@{{activeUnit.name}} </span>| @{{activeUnit.floor_plan.name}}| @{{activeUnit.floor_plan.bdbth}} | @{{activeUnit.sqft}} SQ FT | $@{{activeUnit.rent}} | Avail. @{{activeUnit.date_available}}</p>
    </div>

    <!-- Info PopUp Start -->
    <div class="info-wrap" v-if="!isInfoWrap">
        <div class="header" v-if="activeUnit != null">
            <div class="activeunit">
                <div class="matchlist-title">
                    <div>Unit</div>
                    <div>Plan</div>
                    <div>Available</div>
                    <div>Rent</div>
                    <div>Scheme</div>
                </div>
                <div class="sorticon-warp">
                    <div class="sortico">@{{activeUnit.name}}</div>
                    <div class="sortico">@{{activeUnit.floor_plan.name}}| @{{activeUnit.floor_plan.bdbth}}</div>
                    <div class="sortico">@{{activeUnit.date_available}}</div>
                    <div class="sortico">$@{{activeUnit.rent}}</div>
                    <div class="sortico">@{{activeUnit.scheme.name}}</div>
                </div>
            </div>
            <div class="closeBtn" v-on:click="isInfoWrap = !isInfoWrap">
                <p></p><p>X</p>
            </div>
        </div>
        <!-- Floor Plan Into & Features Starts-->
        <pre>@{{activeUnit.floor_plan.plan_intro}}@{{activeUnit.floor_plan.feature}}</pre>
        <!-- Floor Plan Into & Features Ends-->

        <div class="info-items">
            <div class="info-btn">
                <span @click="specialBtn" :class="[isSpecial ? 'selected' : '']">SPECIALS</span> | 
                <span @click="petBtn" :class="[isPet ? 'selected' : '']">PETS</span> | 
                <span @click="leasingBtn" :class="[isLeasing ? 'selected' : '']">LEASING TERMS</span> | 
                <span @click="parkingBtn" :class="[isParking ? 'selected' : '']">PARKING</span>
            </div>
            <div class="info-data">
                <div v-if="isSpecial">{!! $specials->specials !!}</div>
                <div v-if="isPet">{!! $specials->pet_policy !!}</div>
                <div v-if="isLeasing">{!! $specials->leasing_terms !!}</div>
                <div v-if="isParking">{!! $specials->parking_policy !!}</div>
            </div>
        </div>

        <!-- Schedule & Apply Buttons Start -->
            <div class="sa-btn-wrap">
                <a href="https://google.com" target="_blank">SCHEDULE TOUR</a>
                <a href="https://google.com" target="_blank">APPLY ONLINE</a>
            </div>
        <!-- Schedule & Tour Buttons Start -->

        <!-- Floor Plan Starts -->
            <img :src="assetsUrl + '/img/floor_plans/'+ activeUnit.floor_plan.name + '.png'" alt="Floor Plan">
        <!-- Floor Plan Starts -->
    </div>
    <!-- Info PopUp Start -->

    <div class="touricons"  @click="mapIcon">
        <img  :class="[isFloorPlan ? 'selected' : '']" src="{{asset('frontend/img/m-map-icon.png')}}">
        <img  :class="[!isFloorPlan ? 'selected' : '']" src="{{asset('frontend/img/m-cam-icon.png')}}">
    </div>

    <transition  name="custom-classes-transition"
            enter-active-class="animated fadeIn"
            leave-active-class="animated fadeOut">
        <div class="box sitecontent" v-if="activeUnit !== null" v-show="isFloorPlan">
            
            <div class="floordetail">
                <div class="detail">
                    <div class="quick">
                        <h4>FLOOR</h4>
                        <div v-for="floor in floors" v-if="floor.id>0" class="qbox" :class="{selected : activeUnit.floor_no == floor.id}" @click="floorClicked(floor)">
                            <span>@{{floor.id}}</span>
                            <span>@{{getAvailableUnitsInFloor(floor.id)}} units</span>
                        </div>
                        <div class="closeBtn" v-on:click="isFloorPlan = !isFloorPlan">
                            <p>X</p>
                        </div>

                    </div>
                </div>

                <div class="floor">
                    <img src="{{asset('frontend/img/site-plan-new.jpg')}}" alt="">
                </div>
                <div class="detail">
                    <div class="aptqk" v-if="activeUnit!=null">
                        <div class="name">
                            <h5>APT <span>@{{ activeUnit.name }}</span></h5>
                        </div>
                        <div class="con" >
                            <div class="img-d">
                                <img src="{{asset('frontend/img/thumb-apt.jpg')}}" alt="">
                                <div>
                                    <span>@{{activeUnit.floor_plan.name}} | @{{activeUnit.floor_plan.bdbth}}</span>
                                    <span>627 Sf</span>
                                    <span>@{{ unitAvailability }}</span>
                                    <span>$@{{activeUnit.rent}}</span>
                                    <span>@{{ activeUnit.scheme.name }} Scheme</span>
                                </div>
                            </div>
                            <p @click="isFloorPlan = !isFloorPlan">TAP TO VIEW</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </transition>


<!-- Info Section Ends-->