<div class="matchsearch" :class="listClass">
    <div class="detail">
        <div class="box matchlist">
            <div class="header">
                <span class="circle unitno">@{{filteredUnits.length}}</span>
                <p>HOME MATCHES YOUR SEARCH!</p>
            </div>
            <div class="content" :class="listClass">
                <div id="trHead">
                    <div id="tdApt">APT</div>
                    <div id="tdPlan">Plan</div>
                    <div id="tdAvailable">Available</div>
                    <div id="tdRent">Rent</div>
                    <div id="tdScheme">Scheme</div>
                </div>
                <div class="sorticon-warp">
                    <div class="sortico" @click="sortList('name')"><img :style="getSortRotation('name')" :src="getSortIcon('name')" alt=""></div>
                    <div class="sortico" @click="sortList('floor_plan')"><img :style="getSortRotation('floor_plan')" :src="getSortIcon('floor_plan')" alt=""></div>
                    <div class="sortico" @click="sortList('date_available')"><img :style="getSortRotation('date_available')" :src="getSortIcon('date_available')" alt=""></div>
                    <div class="sortico" @click="sortList('scheme')"><img :style="getSortRotation('scheme')" :src="getSortIcon('scheme')" alt=""></div>
                </div>

                <table class="table table-responsive list" >
                    <tbody>
                    <tr  v-for="unit in filteredUnits" class="trdata" :class="{active : activeUnit.id === unit.id}" @click="unitClicked(unit);">
                        <td>@{{  unit.name }}</td>
                        <td>@{{  (unit.floor_plan)? unit.floor_plan.name + '|' + unit.floor_plan.bdbth : 'N/A' }} </td>
                        <td>@{{ (unit.date_available)? getAvailabilityStr(unit.date_available) : 'N/A'}}</td>
                        <td>@{{ (unit.floor_plan)? unit.scheme.name : 'N/A' }}</td>
                    </tr>
                    <tr v-if="filteredUnits.length === 0">
                        <td colspan="5" class="no-results">No homes match your search, adjust your filters!</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="footer">
                <div v-if="activeUnit !== null" class="header activeunit" :class="listClass">
                    <p>To open the list of homes that match your search click the gray dots! </p>
                </div>
                <div class="sizecontrol">
                    <div class="round min" @click="listClass = 'mini';">
                        <input type="radio" id="sizeMin" name="size" />
                        <label for="sizeMin"></label>
                    </div>

                    <div class="round mid" @click="listClass = 'medium'">
                        <input type="radio" id="sizeMid" name="size" checked />
                        <label for="sizeMid"></label>
                    </div>

                    <div class="round max" @click="listClass = 'large'">
                        <input type="radio" id="sizeMax" name="size" />
                        <label for="sizeMax"></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="box unithighlight" v-if="activeUnit !== null" :class="detailClass">
            <div class="header">
                <span class="circle unitno">@{{activeUnit.name}}</span>
                <p>@{{activeUnit.floor_plan.name}} @{{activeUnit.floor_plan.bdbth}} | @{{ activeUnit.sqft }} SQ FT | @{{unitAvailability}} | $@{{activeUnit.rent}}*</p>
            </div>
            <div class="content matchdetail" :class="detailClass">
                <div class="textarea matchdatawarp" style="">
                    <textarea style="resize: none;" id="matchdata" class="matchdata" readonly>@{{activeUnit.floor_plan.plan_intro}}&#13;&#10;&#13;&#10;@{{  (activeUnit.feature_1!=='')? bulletStr+activeUnit.feature_1+newLineStr : '' }}@{{ (activeUnit.feature_2 !== '')? bulletStr+activeUnit.feature_2+newLineStr : '' }}@{{ (activeUnit.feature_3 !== '')? bulletStr+activeUnit.feature_3+newLineStr : '' }}@{{ getBulletedFeatures(activeUnit.floor_plan.feature) }}
                    </textarea>
                </div>
                <div>
                    @include('layouts.frontend.part.floorplangrid')
                    <img :src="assetsUrl + '/img/floor_plans/'+ activeUnit.floor_plan.name + '.png'" alt="Floor Plan">
                </div>
            </div>
        </div>
        <div class="footer">
            <div v-if="activeUnit !== null" class="header activeunit" :class="detailClass">
                <p>To view the summary and floor plan select a gray dot!</p>
            </div>
            <div class="sizecontrol">
                <div class="round min" @click="detailClass = 'min';">
                    <input type="radio" id="detailSizeMin" name="detailsize" />
                    <label for="detailSizeMin"></label>
                </div>

                <div class="round mid" @click="detailClass = 'mid'">
                    <input type="radio" id="detailSizeMid" name="detailsize"  />
                    <label for="detailSizeMid"></label>
                </div>

                <div class="round max" @click="detailClass = 'max'">
                    <input type="radio" id="detailSizeMax" name="detailsize" checked/>
                    <label for="detailSizeMax"></label>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
