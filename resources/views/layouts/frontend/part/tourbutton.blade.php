<div class="tourbtn">
    <div class="tbtn tour" :class="{selected : !isFloorPlan}" @click="isFloorPlan = !isFloorPlan">
        <span>TOUR | PHOTO</span><img src="{{asset('frontend/img/tour_photo_white.png')}}" alt="">
    </div>
    <div class="tbtn site" :class="{selected : isFloorPlan}" @click="isFloorPlan = !isFloorPlan">
        <span>SITE PLAN</span><img src="{{asset('frontend/img/site-plan-icon.png')}}" alt="" >
    </div>
</div>
