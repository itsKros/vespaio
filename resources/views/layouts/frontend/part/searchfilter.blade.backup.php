
<div class="searchfilter">
    <div class="title">
        <span>FILTER HOMES</span>
    </div>

    <div class="filterbtn">
        <div class="slab openclose" @click="filterBtn">
            <div class="namefilter">
                <span class="opc">@{{filterBtnVal}} ALL</span>
            </div>
        </div>

        <div class="slab floor" >
            <div class="fwrap" v-on:click="isFloorHidden = !isFloorHidden">
                <div class="namefilter">
                    <span>FLOOR</span>
                    <span>All</span>
                </div>
                <i class="fa fa-plus"></i>
            </div>
            <transition  name="custom-classes-transition"
                enter-active-class="animated fadeInUp"
                leave-active-class="animated fadeOutDown">
                <div class="filteritems flrs" v-if="!isHidden || !isFloorHidden">
                    <div class="filter-content" v-for="floor in floors">
                        <p>@{{floor.name}}</p>
                        <div class="round">
                            <input type="checkbox" :id="'floor_'+floor.id" :value="floor.id" v-model="floor.checked" @click="filterClicked(floor, floors)"/>
                            <label :for="'floor_'+floor.id"></label>
                        </div>
                    </div>
                </div>
            </transition>
        </div>


        <div class="slab view">
            <div class="fwrap" v-on:click="isViewHidden = !isViewHidden">
                <div class="namefilter">
                    <span>VIEW</span>
                    <span>All</span>
                </div>
                <i class="fa fa-plus"></i>
            </div>
            
            <transition  name="custom-classes-transition"
                enter-active-class="animated fadeInUp"
                leave-active-class="animated fadeOutDown">
                <div class="filteritems views" v-if="!isHidden || !isViewHidden">
                    <div class="filter-content" v-for="view in views">
                        <p>@{{view.name}}</p>
                        <div class="round">
                            <input type="checkbox" :id="'view_'+view.id" :value="view.id" v-model="view.checked" @click="filterClicked(view, views)"/>
                            <label :for="'view_'+view.id"></label>
                        </div>
                    </div>
                </div>
            </transition>
        </div>
        <div class="slab scheme" >
            <div class="fwrap" v-on:click="isSchemeHidden = !isSchemeHidden">
                <div class="namefilter">
                    <span>SCHEME</span>
                    <span>All</span>
                </div>
                <i class="fa fa-plus"></i>
            </div>
            
            <transition  name="custom-classes-transition"
                enter-active-class="animated fadeInUp"
                leave-active-class="animated fadeOutDown">
                <div class="filteritems schm" v-if="!isHidden || !isSchemeHidden">
                    <div class="filter-content" v-for="scheme in schemes">
                        <p>@{{scheme.name}}</p>
                        <div class="round">
                            <input type="checkbox" :id="'scheme_'+scheme.id" :value="scheme.id" v-model="scheme.checked" @click="filterClicked(scheme, schemes)"/>
                            <label :for="'scheme_'+scheme.id"></label>
                        </div>
                    </div>
                </div>
            </transition>
        </div>
        <div class="slab avail">
            <div class="fwrap" v-on:click="isAvailableHidden = !isAvailableHidden">
                <div class="namefilter">
                    <span>AVAILABLE</span>
                    <span>All</span>
                </div>
                <i class="fa fa-plus"></i>
            </div>
            <transition  name="custom-classes-transition"
                enter-active-class="animated fadeInUp"
                leave-active-class="animated fadeOutDown">
                <div class="filteritems avl" v-if="!isHidden || !isAvailableHidden">
                    <div class="filter-content" v-for="period in periods">
                        <p>@{{period.name}}</p>
                        <div class="round">
                            <input type="checkbox" :id="'period_'+period.id"  :value="period.id" v-model="period.checked" @click="filterClicked(period, periods)"/>
                            <label :for="'period_'+period.id"></label>
                        </div>
                    </div>
                </div>
            </transition>
        </div>
        <div class="slab rent">
            <div class="fwrap" v-on:click="isRentHidden = !isRentHidden">
                <div class="namefilter">
                    <span>RENT</span>
                    <span>All</span>
                </div>
                <i class="fa fa-plus"></i>
            </div>

            <transition  name="custom-classes-transition"
                enter-active-class="animated fadeInUp"
                leave-active-class="animated fadeOutDown">
                <div class="filteritems rnt" v-if="!isHidden || !isRentHidden">
                    <div class="filter-content" v-for="rentRange in rentRanges">
                        <p>@{{rentRange.name}}</p>
                        <div class="round">
                            <input type="checkbox" :id="'rent_range_'+rentRange.id" :value="rentRange.id" v-model="rentRange.checked" @click="filterClicked(rentRange, rentRanges)"/>
                            <label :for="'rent_range_'+rentRange.id"></label>
                        </div>
                    </div>
                </div>
            </transition>
        </div>
    </div>
    
</div>