<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'Frontend\SiteController@index')->name('frontend.home');

//Admin Routes
Route::group(['prefix' => 'admin'], function(){

    Route::name('backend.')->group(function () {

        // Dashboard
        Route::get('/', 'Backend\HomeController@index')->name('home');

        // Users
        Route::get('/users/new_password/{token}','Backend\UsersController@new_password' )->name('users.new_password');
        Route::post('/users/new_password/save','Backend\UsersController@new_password_save' )->name('users.new_password.save');
        Route::post('/users/save','Backend\UsersController@update' )->name('users.save');
        Route::get('/users/destroy/{user}', 'Backend\UsersController@destroy')->name('users.delete');
        Route::resource('users', 'Backend\UsersController')->only(['index', 'store', 'update']);

        // Units
        //Route::get('/units/filter', 'Backend\UnitsController@filter')->name('units.filter');
        Route::post('/units/save','Backend\UnitsController@update')->name('units.save');
        Route::get('units', 'Backend\UnitsController@filter')->name('units.filter');

        // Floor Plans
        Route::post('/floor-plans/save','Backend\FloorPlansController@update' )->name('floor-plans.save');
        Route::resource('floor-plans', 'Backend\FloorPlansController')->only(['index']);

        // Specials
        Route::resource('specials', 'Backend\SpecialsController');

    });

});

